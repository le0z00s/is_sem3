package pl.jrj.db;

import javax.ejb.Remote;
/**
 * @author Jerzy Jaworowski
 */
@Remote
public interface IDbManager {
    
    /**
     * Metoda dokonuje rejestracji użytkownika w systemie 
     * @param hwork numer zadania
     * @param album numer indeksu studenta
     * @return true - zarejestrowano pomyślnie
     */
    public boolean register(int hwork, String album);

}
