
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import pl.jrj.db.IDbManager;

/**
 * Klasa usługi sieciowej zajmującej się procesem zliczania
 * @author Rafał Zbojak
 */
@Path("cnt")
public class Control {
    private static final String jndiString = "java:global/"
           + "ejb-project/DbManager!pl.jrj.db.IDbManager";
    private static final int hwork = 9;
    private static final String indexNumber = "93514";
    
    private static int counter;
    private static int errors;
    private static boolean state;
    
    /**
     * Metoda ustawia stan komponentu w stan zliczania
     */
    @GET
    @Path("/start")
    public void start() {
        if( !getState() ) {
            setState(true);
        }else {
            incrementErrors();
        }
    }
    
    /**
     * Metoda ustaia stan komponentu w stan wstrzymania
     */
    @GET
    @Path("/stop")
    public void stop() {
        if( getState() ) {
            setState(false);
        }else {
            incrementErrors();
        }
    }
    
    /**
     * Metoda zwraca stan licznika
     * @return stan licznika
     */
    @GET
    @Path("/counter")
    @Produces(MediaType.TEXT_PLAIN)
    public String counter() {
        int counter = getCounter();
        return String.format("%d", counter);
    }
    
    /**
     * Metoda zwraca stan licznika błędów
     * @return stan licznika błędów
     */
    @GET
    @Path("/errors")
    @Produces(MediaType.TEXT_PLAIN)
    public String errors() {
        int errors = getErrors();
        return String.format("%d", errors);
    }
    
    /**
     * Metoda inkrementuje licznik o 1 gdy komponent jest w stanie zlicznia
     * W przeciwnym wypadku zwiększa licznik błędów
     */
    @GET
    @Path("/incr")
    public void increment() {
        if( getState() ) {
            incrementCounter();
        }else {
            incrementErrors();
        }
    }
    
    /**
     * Metoda inkrementuje licznik o n gdy komponent jest w stanie zliczania
     * W przeciwnym wypadku zwiększa licznik błędów
     * @param n stan o jaki ma się zwiększyć licznik
     */
    @GET
    @Path("/incr/{n}")
    public void increment(@PathParam( "n" ) int n) {
        if( getState() ) {
            int v = Integer.valueOf(n);
            increment(v);
        }else {
            incrementErrors();
        }
    }
    
    /**
     * Metoda dekrementuje licznik o 1 gdy komponent jest w stanie zlicznia
     * W przeciwnym wypadku zwiększa licznik błędów
     */
    @GET
    @Path("/decr")
    public void decrement() {
        if( getState() ) {
            decrementCounter();
        }else {
            incrementErrors();
        }
    }
    
    /**
     * Metoda dekrementuje licznik o n gdy komponent jest w stanie zliczania
     * W przeciwnym wypadku zwiększa licznik błędów
     * @param n stan o jaki ma się zmniejszyć licznik
     */
    @GET
    @Path("/decr/{n}")
    public void decrement(@PathParam( "n" ) int n) {
        if( getState() ) {
            int v = Integer.valueOf(n);
            decrementCounter(v);
        }else {
            incrementErrors();
        }
    }
    
    /**
     * Konstruktor; Dokonuje rejestracji użytkownika
     * @throws NamingException wyjątek zwracany w momencie gdy
     * nie odnaleziono szukanego komponentu w usłudze nazw
     * @throws Exception wyjątek zwracany gdy proces rejestracji
     * nie przebiegł pomyślnie
     */
    public Control() throws NamingException, Exception{
        if (!register(hwork, indexNumber)) 
            throw new Exception("Błąd podczas rejestracji użytkownika");
    }
    
    /**
     * Metoda dokonuje rejestracji użytkownika w systemie
     * @param hwork numer zadania
     * @param indexNumber numer ndeksu studenta
     * @return true - zarejestrowano pomyślnie
     * @throws NamingException wyjątek zwracany w momencie gdy
     * nie odnaleziono szukanego komponentu w usłudze nazw
     */
    private static boolean register(int hwork, String indexNumber)
                throws NamingException {
        IDbManager manager;
        InitialContext context = new InitialContext();
        
        manager = (IDbManager) context.lookup(jndiString);
        return manager.register(hwork, indexNumber);
    }
    
    /**
     * Metoda zwraca stan licznika
     * @return stan licznika
     */
    private static int getCounter() {
        return counter;
    }
    
    /**
     * Metoda inkrementuje licznik o 1
     */
    private static void incrementCounter() {
        counter++;
    }
    
    /**
     * Metoda inkrementuje stan licznika o n
     * @param n wartość o jaką ma zostać zwiększony licznik
     */
    private static void incrementCounter(int n) {
        counter += n;
    }
    
    /**
     * Metoda dekrementuje licznik o 1
     */
    private static void decrementCounter() {
        counter--;
    }
    
    /**
     * Metoda dekrementuje stan licznika o n
     * @param n wartość o jaką ma zostać zmniejszony licznik
     */
    private static void decrementCounter(int n) {
        counter -= n;
    }
    
    /**
     * Metoda zwraca stan liczika błędów
     * @return stan licznika błędów
     */
    private static int getErrors() {
        return errors;
    }
    
    /**
     * Metoda inkrementuje licznik błędów o 1
     */
    private static void incrementErrors() {
        errors++;
    }
    
    /**
     * Metoda sprawdza w jakim stanie jest komponent
     * @return true - stan zliczania, false - stan wstrzymania 
     */
    private static boolean getState() {
        return state;
    }
    
    /**
     * Metoda ustawia stan komponentu
     * @param s true - stan zliczania, false - stan wstrzymania
     */
    private static void setState(boolean s) {
        state = s;
    }
}
