
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import pl.jrj.db.IDbManager;
/**
 * Komponent typu MDB (Message Driven Bean) nasłuchujący komunikatów
 * tekstowych
 * @author Rafał Zbojak
 */
@MessageDriven(mappedName = "jms/MyQueue", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType",
            propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode",
            propertyValue = "Auto-acknowledge")})
public class MsgBean implements MessageListener {
    
    private static final String JNDI_STRING = "java:global/"
            + "ejb-project/DbManager!pl.jrj.db.IDbManager";
    
    private static final int HWORK = 10;
    private static final String INDEX_NUMBER = "93514";
    
    private static final String CONNECTION_FACTORY_NAME = 
            "jms/ConnectionFactory";
    private static final String TOPIC_NAME = "jms/MyTopic";
    
    private static boolean state;
    private static int counter;
    private static int errors;
    
    /**
     * Metoda umożliwiająca rejestrację użytkownika
     *
     * @param hwork numer zadania
     * @param indexNumber numer albumu
     * @throws NamingException wyjątek zwracany, kiedy nie powiodła się 
     * operacja wyszukiwania potrzebnego komponentu w usłudze nazw
     */
    private static boolean register(int hwork, String indexNumber)
            throws NamingException {
        IDbManager remote;
        InitialContext context = new InitialContext();

        remote = (IDbManager) context.lookup(JNDI_STRING);
        return remote.register(hwork, indexNumber);
    }
    
    /**
     * Metoda obsługuje logikę biznesową aplikacji. Wykonywana w przypadku gdy
     * w kolejce wiadomości pojawi się nowy komunikat. Po wykonaniu metody, 
     * komponent jest gotowy do przetworzenia kolejnego żądania.
     * @param message 
     */
    @Override
    public void onMessage(Message message) {
        try {
            String content = ((TextMessage) message).getText();
            content = content.toLowerCase().trim();
            switch (content) {
                case "start":
                    if(getState()) {
                        incrementErrors();
                    }else {
                        register(HWORK, INDEX_NUMBER);
                        setState(true);
                    }
                    break;
                case "stop":
                    if(getState()) {
                        setState(false);
                    }else {
                        incrementErrors();
                    }
                    break;
                case "counter":
                    respond(String.format("%s/%d", INDEX_NUMBER, counter));
                    break;
                case "errors":
                    respond(String.format("%s/%d", INDEX_NUMBER, errors));
                    break;
                case "incr":
                    if(getState()) {
                        incrementCounter();
                    }else {
                        incrementErrors();
                    }
                    break;
                default:
                    String regex = "incr/-?(\\d)+(\\.+)?(\\d)*";
                    if(content.matches(regex)) {
                        Pattern pattern = Pattern.compile(regex);
                        Matcher match = pattern.matcher(content);
                        
                        if(match.find()) {
                            incrementCounter(Integer
                                    .parseInt(match.group(1)));
                        }else {
                            incrementErrors();
                        }
                    }else {
                        incrementErrors();
                    }
                    break;
            }
        } catch (JMSException ex) {
            Logger.getLogger(MsgBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (NamingException ex) {
            Logger.getLogger(MsgBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Metoda umieszcza w zasobie wiadomość tekstową
     * @param response Wiadomość, która zostanie wysłana do zasobu
     * @throws JMSException wyjątek zwracany gdy nie powiodła się operacja 
     * połączenia z zasobem 
     * @throws NamingException wyjątek zwracany, kiedy nie powiodła się 
     * operacja wyszukiwania potrzebnego komponentu w usłudze nazw
     */
    private void respond(String response)
            throws JMSException, NamingException {
        InitialContext context = new InitialContext();
            TopicConnectionFactory topicConnectionFactory = 
                    (TopicConnectionFactory) context.
                            lookup(CONNECTION_FACTORY_NAME);
            TopicConnection connection = topicConnectionFactory.
                    createTopicConnection();
            connection.start();
            
            TopicSession session = connection.
                    createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            
            Topic topic = (Topic) context.lookup(TOPIC_NAME);
            
            TopicPublisher topicPublisher = session.createPublisher(topic);
            
            TextMessage message = session.createTextMessage();
            
            message.setText(response);
            
            topicPublisher.publish(message);
            
            connection.close();
    }
    
    /**
     * Metoda zwraca stan licznika
     * @return 
     */
    private static int getCounter() {
        return counter;
    }
    
    /**
     * Metoda zwiększa stan licznika o zadaną wartość
     * @param counter stan o jaki ma się zmienić licznik
     */
    private static void incrementCounter(int counter) {
        MsgBean.counter += counter;
    }
    
    /**
     * Metoda zwiększa stan licznika o 1
     */
    private static void incrementCounter() {
        MsgBean.counter++;
    }

    /**
     * Metoda zwraca liczbę błędów
     * @return 
     */
    private static int getErrors() {
        return errors;
    }
    
    /**
     * Metoda zwiększa liczbę błędów o 1
     */
    private static void incrementErrors() {
        MsgBean.errors++;
    }

    /**
     * Metoda sprawdza czy komponent ustawiono w stan zliczania
     * @return true - stan zliczania, false - stan wstrzymania
     */
    private static boolean getState() {
        return state;
    }

    /**
     * Metoda ustawia komponent w stan zliczania (true)
     * lub w stan wstrzymania (false)
     */
    private static void setState(boolean state) {
        MsgBean.state = state;
    }    
}
