import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rafał Zbojak
 */
public class Block extends HttpServlet {
    
    Map<Point, Double> grains;
    
    /**
     * Helper class representing point in 3D space
     */
    private class Point{
        private double x, y, z;
        public Point(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        /**
         * Getter for X coordinate
         * @return X coordinate
         */
	public double getX () {
		return x;
	}
        /**
         * Getter for Y coordinate
         * @return Y coordinate
         */
        public double getY () {
		return y;
	}
        /**
         * Getter for Z coordinate
         * @return Z coordinate
         */
        public double getZ () {
		return z;
	}
    }
    
    /**
     * Helper class representing cylinder
     */
    private class Cylinder {
        private double r, h;
        /**
         * Cylinder constructor
         * @param r radius of cylinder
         * @param h height of cylinder
         */
        public Cylinder(double r, double h) {
            this.r = r;
            this.h = h;
        }
        
        /**
         * Getter for R
         * @return R
         */
	public double getR() {
		return r;
	}
        
        /**
         * Getter for H
         * @return H
         */
	public double getH() {
		return h;
	}
        
        /**
         * Getter for Cylinder volume
         * @return R coordinate
         */
	public double getVolume() {
            return Math.PI * Math.pow(r, 2) * h;
	}
        
        /**
         * Method determines if point belongs to cylinder
         * @param point point to test
         * @return point belongs/doesn't belong to cylinder
         */
        public boolean isCylinder(Point point) {
            if( Math.pow(point.getX(), 2) 
                    + Math.pow(point.getY(), 2)
                    <= Math.pow(r, 2) &&
                    point.getZ() <= h) {
                return true;
            }
            return false;
        }
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        double x = Double.parseDouble("x");
        double y = Double.parseDouble("y");
        double z = Double.parseDouble("z");
        double r = Double.parseDouble("r");
        
        Point point = new Point(x, y, z);
        addGrain(point, r);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter printWriter = response.getWriter();
        response.setContentType("text/html");
        double r = Double.parseDouble("r");
        double h = Double.parseDouble("h");
        double c = Double.parseDouble("c");
        double g = Double.parseDouble("g");
        
        Cylinder cylinder = new Cylinder(r, h);
        
        double grainRatio = monteCarlo(cylinder);
        double mass = cylinder.getVolume() * ( (grainRatio * g) + ((1.0f - grainRatio) * c) );
        grains.clear();
        printWriter.format("%.3f", mass);
        printWriter.close();
    } 
    
    /**
     * Adds grain to map
     * @param point point in 3D space
     * @param radius radius of sphere
     */
    private void addGrain(Point point, Double radius) {
        if(grains == null) {
            grains = new HashMap<Point, Double>();
        }
        grains.put(point, radius);
    }
    
    /**
     * Method generates a random sample between -r and r 
     * @param range Size of cube edge
     */
    
    /**
     * Method checks if the point belongs to grain
     * @return true if point belongs to grain, false if not 
    */
    private boolean isGrain(Point point) {
        for( Map.Entry<Point,Double> grain : grains.entrySet() ) {
            if( Math.pow((point.getX() - grain.getKey().getX()), 2)
                    + Math.pow((point.getY() - grain.getKey().getY()), 2)
                    + Math.pow((point.getZ() - grain.getKey().getZ()), 2)
                    <= Math.pow(grain.getValue(), 2)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Method generates a random sample
     * @param radius radius of sphere/height of cylinder
     * @param isHeight determines if it returns sample for height of cylinder
     * @return random sample for fingle coordinate
     */
    private double getRandom(double radius, boolean isHeight) {
        SecureRandom random = new SecureRandom();
        double sample = 0;
        if(isHeight) {
            sample = (random.nextDouble()*radius);
        } else {
            sample = ( (random.nextDouble() * 2 * radius) - radius );
        }
        return sample;
    }
    
    /**
     * Method generates a random point sample
     * @param cylinder bounds for random point generator
     * @return point within a cylinder
     */
    private Point getRandom( Cylinder cylinder ) {
        Point point;
        do{
            double x = getRandom(cylinder.getR(), false);
            double y = getRandom(cylinder.getR(), false);
            double z = getRandom(cylinder.getH(), true);
            
            point = new Point(x, y, z);
        }while(!cylinder.isCylinder(point));
        return point;
    }    
    
    private double monteCarlo(Cylinder cylinder) {
        int grainHit = 0;
        int precision = 100000;
        
        for(int i = 0; i < precision; i++) {
            Point randomPoint = getRandom(cylinder);
            if( isGrain(randomPoint) ){
                grainHit++;
            }
        }

        return (double) (grainHit / precision);
    }
}