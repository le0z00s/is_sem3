package pl.jrj.db;

import javax.ejb.Remote;
/**
 * @author Rafał Zbojak
 */
@Remote
public interface IDbManager {
    public boolean register(int hwork, String album);
}
