
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pl.jrj.db.IDbManager;
import java.util.List;

/**
 * 
 * @author Rafał Zbojak
 */
@Path("country")
public class Country {
    private static final String archivePath = "http://www.stat.gov.pl/broker/"
            + "access/prefile/downloadPreFile.jspa?id=1616";
    private static final String jndiString = "java:global/"
            + "ejb-project/DbManager!pl.jrj.db.IDbManager";
    private static final String indexNumber = "93514";
    private static final int hwork = 8;
    
    @Context
    private UriInfo context;
    
    /**
     * Domyślny konstruktor bezargumentowy
     * @throws NamingException
     * @throws Exception 
     */
    public Country() throws NamingException, Exception { 
        if (!register(hwork, indexNumber)) throw new Exception("Błąd podczas "
                + "rejestracji użytkownika");
    }
    
    /**
     * Metoda umożliwia rejestrację studenta
     * @param hwork numer zadania
     * @param indexNumber numer albumu studenta
     * @return true jeśli rejestracja przebiegła pomyślnie
     * @throws NamingException wyjątek wyrzucany w momencie
     * nieznalezienia potrzebnego komponentu w usłudze nazw
     */
    private static boolean register(int hwork, String indexNumber) 
        throws NamingException {
        IDbManager manager;
        InitialContext context = new InitialContext();
        
        manager = (IDbManager) context.lookup(jndiString);
        return manager.register(hwork, indexNumber);
    }
    
    /**
     * Metoda pobiera plik XML zawierający dane terytorialne
     * @return dokument z podziałem terytorialnym
     * @throws MalformedURLException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException 
     */
    private Document getXMLFile() throws MalformedURLException, 
            IOException, ParserConfigurationException, SAXException {
        URL archiveURL = new URL(archivePath);
        URL xmlURL = new URL(String.format("jar:%s!/TERC.xml", archiveURL));
        InputStream stream = xmlURL.openStream();
        DocumentBuilderFactory factory = 
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        return builder.parse(stream);
    }
    
    /**
     * Metoda zwraca korzeń dokumentu z pliku XML
     * @return korzeń dokumentu
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    private Element getDomRoot() throws ParserConfigurationException,
            SAXException, IOException {
        Document document = getXMLFile();
        return document.getDocumentElement();
    }
    
    /**
     * Metoda zwraca listę województw
     * @return lista województw
     */
    private List<Unit> getProvince() {
        List<Unit> provinces = new ArrayList<Unit>();
        try {
            Element root = getDomRoot();
            NodeList rows = root.getElementsByTagName("row");
            int rowsCount = rows.getLength();
            for(int i = 0 ; i < rowsCount ; i++) {
                Node row = rows.item(i);
                NodeList cols = ((Element) row).getElementsByTagName("col");
                if( !cols.item(0).getTextContent().isEmpty() &&
                        cols.item(1).getTextContent().isEmpty()) {
                    provinces.add(new Unit(cols.item(4).getTextContent()));;
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        
        return provinces;
    }
    
    /**
     * Metoda zwraca liczbę województw
     * @return liczba województw
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getProvinceCount() {
        int provinces = getProvince().size();
        
        return String.format("%d", provinces);
    }
    
    /**
     * Metoda zwraca listę województw
     * @return Obiekt JSON z listą województw
     */
    @GET
    @Path("/L")
    @Produces(MediaType.APPLICATION_JSON)
    public String getProvinceList() {
        Gson gson = new GsonBuilder().create();
        
        String json = gson.toJson(getProvince());
        
        return json;
    }
    
    /**
     * Metoda zwraca listę powiatów w wjewództwie
     * @param province kod województwa
     * @return lista powiatów w województwie
     */
    private List<Unit> getCounty(String province) {
        List<Unit> counties = new ArrayList<Unit>();
        
        try {
            Element root = getDomRoot();
            NodeList rows = root.getElementsByTagName("row");
            int rowsCount = rows.getLength();
            for( int i = 0 ; i < rowsCount ; i++) {
                Node row = rows.item(i);
                NodeList cols = ((Element) row).getElementsByTagName("col");
                if (cols.item(0).getTextContent().equals(province) &&
                        !cols.item(1).getTextContent().isEmpty() &&
                        cols.item(2).getTextContent().isEmpty()) {
                    counties.add(new Unit(cols.item(4).getTextContent()));
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        
        return counties;
    }
    
    /**
     * Metoda zwraca liczbę powiatów w województwie
     * @param province kod województwa
     * @return liczba powiatów w województwie
     */
    @GET
    @Path("{province : \\d{2}}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCountyCount(
            @PathParam("province") String province) {
        int counties = getCounty(province).size();
        
        return String.format("%d", counties);
    }
    
    /**
     * Metoda zwraca listę powiatów w województwie
     * @param province kod województwa
     * @return liczba powiatów w województwie
     */
    @GET
    @Path("{province : \\d{2}}/L")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCountyList(
            @PathParam("province") String province) {
        Gson gson = new GsonBuilder().create();
        
        String json = gson.toJson(getCounty(province));
        
        return json;
    }
    
    /**
     * Metoda zwraca listę gmin w powiecie
     * @param province kod województwa
     * @param county kod powiatu
     * @return lista gmin
     */
    private List<Unit> getCommune(String province, String county) {
        List<Unit> communes = new ArrayList<Unit>();
        try {
            Element root = getDomRoot();
            NodeList rows = root.getElementsByTagName("row");
            int rowsCount = rows.getLength();
            for( int i = 0 ; i < rowsCount ; i++) {
                Node row = rows.item(i);
                NodeList cols = ((Element) row).getElementsByTagName("col");
                if (cols.item(0).getTextContent().equals(province) &&
                        cols.item(1).getTextContent().equals(county) &&
                        !cols.item(2).getTextContent().isEmpty()) {
                    communes.add(new Unit(cols.item(4).getTextContent()));
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        return communes;
    }
    
    /**
     * Metoda zwraca liczbę gmin w powiecie
     * @param province kod województwa
     * @param county kod powiatu
     * @return liczba gmin w województwie
     */
    @GET
    @Path("{province : \\d{2}}{county : \\d{2}}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCommuneCount(
            @PathParam("province") String province,
            @PathParam("county") String county) {
        int communes = getCommune(province, county).size();
        
        return String.format("%d", communes);
    }
    
    /**
     * Metoda zwraca listę gmin w powiecie
     * @param province kod województwa
     * @param county kod powiatu
     * @return liczba gmin w województwie
     */
    @GET
    @Path("{province : \\d{2}}{county : \\d{2}}/L")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCommuneList(
            @PathParam("province") String province,
            @PathParam("county") String county) {
        Gson gson = new GsonBuilder().create();
        
        String json = gson.toJson(getCommune(province, county));
        
        return json;
    }
    
    /**
     * Metoda zwraca liczbę gmin w powiecie
     * @param province kod województwa
     * @param county kod powiatu
     * @return liczba gmin w województwie
     */
    @GET
    @Path("{province : \\d{2}}/{county : \\d{2}}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCommuneCountSlash(
            @PathParam("province") String province,
            @PathParam("county") String county) {
        return getCommuneCount(province, county);
    }
    
    /**
     * Metoda zwraca listę gmin w powiecie
     * @param province kod województwa
     * @param county kod powiatu
     * @return liczba gmin w województwie
     */
    @GET
    @Path("{province : \\d{2}}/{county : \\d{2}}/L")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCommuneListSlash(
            @PathParam("province") String province,
            @PathParam("county") String county) {
        return getCommuneList(province, county);
    }
    
    public class Unit {
        private String nazwa;

        /**
         * Getter nazwy jednostki administracyjnej
         * @return nazwa jednostki administracynej
         */
        public String getNazwa() {
            return nazwa;
        }

        /**
         * Setter nazwy jednostki administracyjnej
         * @param nazwa 
         */
        public void setNazwa(String nazwa) {
            this.nazwa = nazwa;
        }
        
        /**
         * Konstruktor publiczny
         */
        public Unit(String nazwa) {
            this.nazwa = nazwa;
        }
    }
}
