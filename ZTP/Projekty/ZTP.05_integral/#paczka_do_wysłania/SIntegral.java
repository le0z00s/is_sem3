
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import pl.jrj.fnc.IFunctMonitor;

/**
 * Komponent oblicza wartość całki podwójnej po obszarze D=[0,a],[0,b]
 * @author Rafał Zbojak
 */
@Stateless
public class SIntegral implements ISIntegralRemote {

    private static final String PATH = 
            "java:global/ejb-project/FunctMonitor!pl.jrj.fnc.IFunctMonitor";
    
    /**
     * Metoda zwraca wartość całki pobranej z komponentu {@link IFunctMonitor}
     * Obliczona wartość stanowi objętość bryły opisanej 
     * funkcją dwóch zmiennych X i Y.
     * Dzieli bryłę opisaną funkcją na prostopadłościany, a następnie
     * sumuje objętość każdego z opisanych prostopadłościanów.
     * @param a granica przedziału dla współrzędnej Y
     * @param b granica przedziału dla współrzędnej X
     * @param n dokładność całkowania
     * @return wartość obliczonej całki
     */
    @Override
    public double solve(int a, int b, int n) {
        double result = 0;
        try{
            InitialContext context = new InitialContext();
            IFunctMonitor function = 
                    (IFunctMonitor) context.lookup(PATH);
            
            int part = 10 * n;
            
            double xPart = (double) (b / part);
            double yPart = (double) (a / part);
            
            result = xPart;
            
            for ( int i = 0 ; i < part ; i++ ) {
                for ( int j = 0; j < part ; j++ ){
                    double z = function.f(xPart * i, yPart * j);
                    z += function.f(xPart * (i + 1), yPart * j);
                    z += function.f(xPart * i, yPart * (j + 1));
                    z += function.f(xPart * (i +1), yPart * (j + 1));
                    z /= 4; //średnia wartość funkcji dla czterech punktów
                    
                    result += xPart * yPart * z;
                }
            }
            
        } catch (NamingException ex) {
            Logger.getLogger(SIntegral.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
