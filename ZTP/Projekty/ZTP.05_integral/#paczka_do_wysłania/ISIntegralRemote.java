
import javax.ejb.Remote;
import pl.jrj.fnc.IFunctMonitor;
/**
 * Komponent oblicza wartość całki podwójnej po obszarze D=[0,a],[0,b]
 * @author Rafał Zbojak
 */
@Remote
public interface ISIntegralRemote {
    /**
     * Metoda zwraca wartość całki pobranej z komponentu {@link IFunctMonitor} 
     * @param a granica przedziału dla współrzędnej Y
     * @param b granica przedziału dla współrzędnej X
     * @param n dokładność całkowania
     * @return wartość obliczonej całki
     */
    public double solve(int a, int b, int n);
}
