/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet wczytujący 
 * @author Rafał Zbojak
 */
public class Solver extends HttpServlet {
    private static final long serialVersionUID = -1L;
    
    private static final String PATH = "java:module/SIntegral";
    private int n;//dokładność całkowania
    private int a;//granica przedziału dla współrzędnej Y
    private int b;//granica przedziału dla współrzędnej X
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            InitialContext context = new InitialContext();
            ISIntegralRemote integral = 
                    (ISIntegralRemote) context.lookup(PATH);
            
            String nString = request.getParameter("n");
            String aString = request.getParameter("a");
            String bString = request.getParameter("b");
            
            n = nString != null ? Integer.parseInt(nString) : 0;
            a = nString != null ? Integer.parseInt(aString) : 0;
            b = nString != null ? Integer.parseInt(bString) : 0;
            
            out.print( integral.solve(a, b, n) );
        } catch (NamingException ex) {
            Logger.getLogger(Solver.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
