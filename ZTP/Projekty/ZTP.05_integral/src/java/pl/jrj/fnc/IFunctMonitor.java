package pl.jrj.fnc;

import javax.ejb.Remote;

/**
 * @author Jerzy Jaworowski
 */
@Remote
public interface IFunctMonitor {

    public double f(double x, double y);
}
