
import java.sql.Connection;
import java.sql.SQLException;
import javax.ejb.Remote;

/**
 * Komponent EJB zawierający metody pobrania danych z zewnętrznego 
 * repozytorium oraz obliczenia objętości graniastosłupa o podstawie P. 
 * Pole podstawy graniastosłupa jest obliczane za pomocą algorytmu Grahama, 
 * która służy do wyznaczenia wypukłej otoczki stworzonej z danych punktów 
 * oraz odpowiednio zastosowanego wzoru Gaussa na obliczenie 
 * pola powierzchni z posiadanych współrzędnych.
 * @author Rafał Zbojak
 */
@Remote
public interface IBlockRemote {
    /**
     * Metoda odczytuje listę punków z zewnętrznego repozytorium danych, 
     * a następnie zlicza elementy oraz dodaje odczytane punkty do listy. 
     * Parametr z każdego z punktów jest sumowany w celu późniejszego 
     * obliczenia wysokości graniastosłupa.
     * @param conn połączenie z bazą danych
     * @param table nazwa tabeli, z której wczytane zostaną dane.
     * @throws SQLException
     */
    public void getData(Connection conn, String table) throws SQLException;
    
    /**
     * Metoda oblicza objętość graniastosłupa za pomocą algorytmu Grahama. 
     * W pierwszym kroku sprawdza czy lista punktów zawiera przynajmniej 
     * trzy elementy, a następnie sortuje listę punktów poprzez wykonanie 
     * prywatnej metody sortByPolarCoord(n). Po posortowaniu wyznaczona 
     * zostaje otoczka wypukła poprzez wywołanie prywatnej 
     * metody findConvex(n). Ostatnim etapem jest obliczenie pola podstawy 
     * oraz objętości graniastosłupa.
     * @return Objętość graniastosłupa.
     */
    public double caldulateVolume();
}
