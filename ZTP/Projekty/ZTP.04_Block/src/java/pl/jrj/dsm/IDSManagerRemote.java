package pl.jrj.dsm;

import javax.ejb.Remote;

/**
 *
 * @author Jerzy Jaworowski
 */
@Remote
public interface IDSManagerRemote {
    public String getDS();
} 
