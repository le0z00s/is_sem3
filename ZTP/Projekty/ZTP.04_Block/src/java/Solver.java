import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import pl.jrj.dsm.IDSManagerRemote;

/**
 * Servlet odpowiedzialny za przekazanie informacji o nazwie tabeli 
 * źródła danych potrzebnego do przeprowadzenia obliczeń do komponentu 
 * Block. Servlet otrzymuje przekazaną w żądaniu (url) jako parametr t 
 * informację o nazwie tabeli źródła danych, następnie łączy się z komponentem
 * DSManager uzyskując informację o nazwie źródła danych pod którą źródło 
 * zarejestrowane zostało w usłudze JNDI. Po wykonaniu powyższych czynności 
 * przekazuje otrzymane dane do komponentu Block oraz drukuje 
 * na wyjściu otrzymany wynik.
 * @author Rafał Zbojak
 */
public class Solver extends HttpServlet {

    @EJB(mappedName = 
            "java:global/ejb-project/DSManager!pl.jrj.dsm.IDSManagerRemote")
    private IDSManagerRemote dsmr;
    
    @EJB
    IBlockRemote block;
    
    /**
     * Processes requests for both HTTP 
     * <code>GET</code> and <code>POST</code> methods.
     *
     * @param req servlet request
     * @param res servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest req, 
            HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = res.getWriter()) {
            
            double result;
            
            String ds = dsmr.getDS();
            String table = req.getParameter("t");
            
            DataSource dataSource = (DataSource) new InitialContext().lookup(ds);
            Connection conn = dataSource.getConnection();
            
            block.getData(conn, table);
            result = block.caldulateVolume();
            
            out.format("%.5f", result);
            
        } catch (NamingException ex) {
            Logger.getLogger(Solver.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param req servlet request
     * @param res servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        processRequest(req, res);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param req servlet request
     * @param res servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        processRequest(req, res);
    }
}
