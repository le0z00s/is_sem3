
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import javax.ejb.Stateful;

/**
 * Komponent EJB zawierający metody pobrania danych z zewnętrznego repozytorium
 * oraz obliczenia objętości graniastosłupa o podstawie P. Pole podstawy
 * graniastosłupa jest obliczane za pomocą algorytmu Grahama, która służy do
 * wyznaczenia wypukłej otoczki stworzonej z danych punktów oraz odpowiednio
 * zastosowanego wzoru Gaussa na obliczenie pola powierzchni z posiadanych
 * współrzędnych.
 *
 * @author Rafał Zbojak
 */
@Stateful
public class Block implements IBlockRemote {

    private final Stack<Point> pointStack = new Stack<Point>();
    private final List<Point> points = new ArrayList<Point>();
    private double height;
    
    /**
     * Metoda odczytuje listę punków z zewnętrznego repozytorium danych, 
     * a następnie zlicza elementy oraz dodaje odczytane punkty do listy. 
     * Parametr z każdego z punktów jest sumowany w celu późniejszego 
     * obliczenia wysokości graniastosłupa.
     * @param conn połączenie z bazą danych
     * @param table nazwa tabeli, z której wczytane zostaną dane.
     * @throws SQLException 
     */
    @Override
    public void getData(Connection conn, String table) throws SQLException {
        String queryString = String.format("SELECT * FROM %s", table);
        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery(queryString);
        
        double x, y, z;
        while(result.next()) {
            x = result.getDouble(2);
            y = result.getDouble(3);
            z = result.getDouble(4);
            
            this.height += z;
            this.points.add(new Point(x, y, z));
        }
        
        this.height /= this.points.size();
    }
    
    /**
     * Metoda oblicza objętość graniastosłupa za pomocą algorytmu Grahama. 
     * W pierwszym kroku sprawdza czy lista punktów zawiera przynajmniej 
     * trzy elementy, a następnie sortuje listę punktów poprzez wykonanie 
     * prywatnej metody sortByPolarCoord(n). Po posortowaniu wyznaczona 
     * zostaje otoczka wypukła poprzez wywołanie prywatnej 
     * metody findConvex(n). Ostatnim etapem jest obliczenie pola podstawy 
     * oraz objętości graniastosłupa.
     * @return Objętość graniastosłupa.
     */
    @Override
    public double caldulateVolume() {
        if( this.points.size() < 3 ) {
            throw new RuntimeException("At least 3 points are required");
        }
        
        sortByPolarCoord();
        //dodaj do stosu pierwsze dwa punkty
        for (int i = 0; i < 2; i++) {
            this.pointStack.push(this.points.get(i));
        }
        findConvex();
        
        double area = calculateArea();
        
        return area;
    }

    /**
     * Metoda sortuje po współrzędnych biegunowych.
     */
    private void sortByPolarCoord() {
        //sortowanie po współrzędnych
        Collections.sort(this.points);
        int n = this.points.size();
        //wyliczenie katow dla wsp. biegunowych
        for (int a = 1; a < n; a++) {
            this.points.get(a).calcAngle(this.points.get(0));
        }
        //posortowanie wedlug katow wsp. biegunowych
        Collections.sort(this.points);
    }

    /**
     * Metoda buduje otoczkę wypukłą z listy odczytanych punktów
     */
    private void findConvex() {
        int n = this.points.size();
        for (int i = 2; i < n; i++) {
            Point top = this.pointStack.pop();
            int ccw = ccw(pointStack.peek(), top, this.points.get(i));
            switch (ccw) {
                case 1:
                    this.pointStack.push(top);
                    this.pointStack.push(points.get(i));
                    break;
                case -1:
                    i--;
                    break;
                default:
                    this.pointStack.push(points.get(i));
                    break;
            }
        }
    }

    /**
     * Metoda licząca pole wielokata wypukłego ze wzoru Gaussa
     */
    private double calculateArea() {
        double area = 0;
        int n = this.pointStack.size() - 1;
        area += ( this.pointStack.elementAt(0).getY()
                * ( this.pointStack.elementAt(1).getX()
                - this.pointStack.elementAt(n).getX() ) );
        
        for (int g = 1; g < n; g++) {
            area += ( this.pointStack.elementAt(g).getY()
                    * ( this.pointStack.elementAt(g + 1).getX()
                    - this.pointStack.elementAt(g - 1).getX() ) );
        }
        
        area += ( this.pointStack.elementAt(n).getY()
                * ( this.pointStack.elementAt(0).getX()
                - this.pointStack.elementAt(n - 1).getX() ) );
        area *= -0.5;
        this.pointStack.clear();
        
        return area;
    }

    /**
     * Metoda służaca do sprawdzania położenia punktów, czy są ułożone
     * przeciwnie do ruchu wskazówek zegara
     *
     * @param a - Punkty początkowy
     * @param b - Punkt środkowy
     * @param c - Punkt końcowy
     * @return 0 - współliniowe, -1 - cw, 1 - ccw
     */
    private int ccw(Point a, Point b, Point c) {
        double turn = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);

        if (turn > 0) {
            return 1; //przeciwnie do wskazówek zegara
        } else if (turn < 0) {
            return -1; //zgodnie ze wskazówkami 
        }
        return 0; //współliniowe
    }

    /**
     * Klasa pomocnicza przechowująca informacje dotyczące punktu w 
     * przestrzeni 3D
     */
    private class Point implements Comparable<Point> {

        private double angle;
        private double x;
        private double y;
        private double z;

        /**
         * Konstruktor dla punktu
         *
         * @param x współrzędna X
         * @param y współrzędna Y
         * @param z współrzędna Z
         */
        public Point(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.angle = 0;
        }

        /**
         * Mutuator dla kąta
         *
         * @return kąt
         */
        public double getAngle() {
            return angle;
        }

        /**
         * Mutuator dla współrzędnej X
         *
         * @return współrzędna X
         */
        public double getX() {
            return x;
        }

        /**
         * Mutuator dla współrzędnej Y
         *
         * @return współrzędna Y
         */
        public double getY() {
            return y;
        }

        /**
         * Mutuator dla współrzędnej Z
         *
         * @return współrzędna Z
         */
        public double getZ() {
            return z;
        }

        /**
         * Metoda obliczajaca kat alfa w wsp. biegunowych
         */
        void calcAngle(Point o) {
            double dx = x - o.x;
            double dy = y - o.y;

            double a = Math.atan2(dy, dx);
            if (a < 0) {
                a += Math.PI * 2;
            }
            this.angle = a;
        }

        /**
         * Komparator dla obiektów klasy
         *
         * @param o obiekt do którego nastąpi porównanie
         * @return -1 gdy mniejsze, 1 gdy większe
         */
        @Override
        public int compareTo(Point o) {
            //na poczatek porownujemy po kacie alfa
            if (this.angle < o.getAngle()) {
                return -1;
            } else if (this.angle > o.getAngle()) {
                return 1;
            }
            return compareByYCoord(o);
        }

        /**
         * Komparator dla obiektów klasy. Porównuje po współrzędnej Y.
         *
         * @param o obiekt do którego nastąpi porównanie
         * @return -1 gdy mniejsze, 1 gdy większe
         */
        private int compareByYCoord(Point o) {
            if (this.y < o.getY()) {
                return -1;
            } else if (this.y > o.getY()) {
                return 1;
            }
            return compareByXCoord(o);
        }

        /**
         * Komparator dla obiektów klasy. Porównuje po współrzędnej X.
         *
         * @param o obiekt do którego nastąpi porównanie
         * @return -1 gdy mniejsze, 1 gdy większe
         */
        private int compareByXCoord(Point o) {
            if (this.x < o.getX()) {
                return -1;
            } else if (this.x > o.getX()) {
                return 1;
            }
            return 0;
        }
    }
}
