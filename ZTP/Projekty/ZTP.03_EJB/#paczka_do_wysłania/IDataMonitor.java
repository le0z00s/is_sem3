package pl.jrj.data;

import javax.ejb.Remote;

/**
 * Interface for EJB component.
 * @author Jerzy Jaworowski
 */
@Remote
public interface IDataMonitor {

    public boolean hasNext();

    public double next();
}
