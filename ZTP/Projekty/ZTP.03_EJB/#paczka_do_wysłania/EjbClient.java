
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import pl.jrj.data.IDataMonitor;

/**
 * Klasa odpowiedzialna za pobranie danych oraz obliczenie momentu bezwładności
 * dla zbioru punktów materialnych reprezentowanych przez odczytane dane.
 *
 * @author Rafał Zbojak
 */
public class EjbClient {

    private double[] line;
    private List<Point> points;
    private Point startPoint;
    private Point endPoint;
    private Point vector;

    /**
     * Leniwe inicjalizowanie zmiennych
     */
    private void init() {
        if (line == null) {
            line = new double[3];
        }

        if (points == null) {
            points = new ArrayList<Point>();
        }
    }
    /**
     * Metoda wprowadza wektor osi obrotu.
     */
    private void initLine() {
        
        this.startPoint = new Point(0, 0, 0, 0);
        
        double x = 1.0;
        double y = -2.0 * this.line[0] / this.line[1];
        double z = this.line[0] / this.line[2];
        this.endPoint = new Point(x, y, z, 0.0);
        
        //obliczenie wektora
        x = this.endPoint.getX() - this.startPoint.getX();
        y = this.endPoint.getY() - this.startPoint.getY();
        z = this.endPoint.getZ() - this.startPoint.getZ();
        
        this.vector = new Point(x, y, z, 0);
    }
    /**
     * Metoda oblicza odległość wybranego punktu od osi obrotu.
     * @param point
     * @return 
     */
    private double getDistance(Point point) {
        Point distance = new Point();
        distance.setX(Math.abs(this.startPoint.getX() - point.getX()));
        distance.setY(Math.abs(this.startPoint.getY() - point.getY()));
        distance.setZ(Math.abs(this.startPoint.getZ() - point.getZ()));
        
        Point scalar = new Point();
        scalar.setX(distance.getY() * vector.getZ() 
                - vector.getY() * distance.getZ());
        scalar.setY(distance.getX() * vector.getZ() 
                - vector.getX() * distance.getZ());
        scalar.setZ(distance.getX() * vector.getY() 
                - vector.getX() * distance.getY());
        
        double r = Math.sqrt( Math.pow(scalar.getX(), 2) 
                + Math.pow(scalar.getY(), 2) 
                + Math.pow(scalar.getZ(), 2));
        r /= Math.sqrt( Math.pow(vector.getX(), 2) 
                + Math.pow(vector.getY(), 2) 
                + Math.pow(vector.getZ(), 2));
        
        return r;
    }
    /**
     * Metoda oblicza moment bezwładności listy punktów przekazanych jako 
     * parametr funkcji.
     * @param points
     * @return 
     */
    private double getMoI(List<Point> points) {
        double inertia = 0;
        double distance;
        for ( Point point : points ) {
            distance = getDistance(point);
            inertia += point.getMass() * Math.pow(distance, 2);
        }
        return inertia;
    }
    /**
     * Główna metoda klasy. Pobiera dane z wykorzystaniem metod komponentu
     * DataMonitor. Do zestawienia połączenia z komponentem użyto mechanizmu
     * Portable Global JNDI Names. Pobiera listę zmiennych typu double oraz je
     * interpretuje. Następnie oblicza współrzędne dla prostej. 
     * Oblicza moment bezwładności odczytanego zbioru 
     * względem osi obrotu i drukuje na wyjściu wynik
     * zaokrąglony do piątego miejsca poprzecinku.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EjbClient client = new EjbClient();
        String jndiString = 
                "java:global/ejb-project/DataMonitor!pl.jrj.data.IDataMonitor";
        try {
            IDataMonitor monitor = (IDataMonitor) new InitialContext()
                    .lookup(jndiString);
            
            client.init();
            int counter1 = 0;
            int counter2;
            Point point = new Point();
            while(monitor.hasNext()) {
                ++counter1;
                if(counter1 < 4) {
                    client.line[counter1 - 1] = monitor.next();
                }else {
                    counter2 = (counter1 - 3)%4;
                    switch (counter2) {
                        case 0:
                            point.setMass(monitor.next());
                            client.points.add(point);
                            break;
                        case 1:
                            point.setX(monitor.next());
                            break;
                        case 2:
                            point.setY(monitor.next());
                            break;
                        case 3:
                            point.setZ(monitor.next());
                            break;
                    }
                }
            }
            
            client.initLine();
            double out = client.getMoI(client.points);
            System.out.format("%.5f", out);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    /**
     * Klasa pomocnicza reprezentująca pojedynczy punkt materialny. Zawiera pola
     * reprezentujące współrzędne punktu w przestrzeni trójwymiarowej oraz jego
     * masę. Ponadto posiada mutatory i accessory dla każdego z pól.
     */
    private static class Point {

        private double x;
        private double y;
        private double z;
        private double mass;
        /**
         * Akcesor dla współrzędnej X punktu
         *
         * @return x współrzędna X
         */
        public double getX() {
            return x;
        }
        /**
         * Mutuator dla współrzędnej X
         *
         * @param x współrzędna X
         */
        public void setX(double x) {
            this.x = x;
        }
        /**
         * Akcesor dla współrzędnej Y punktu
         *
         * @return y współrzędna Y
         */
        public double getY() {
            return y;
        }
        /**
         * Mutuator dla współrzędnej Y
         *
         * @param y współrzędna Y
         */
        public void setY(double y) {
            this.y = y;
        }
        /**
         * Akcesor dla współrzędnej Z punktu
         *
         * @return z współrzędna Z
         */
        public double getZ() {
            return z;
        }
        /**
         * Mutuator dla współrzędnej Z
         *
         * @param z współrzędna Z
         */
        public void setZ(double z) {
            this.z = z;
        }
        /**
         * Akcesor dla masy punktu
         *
         * @return masa punktu materialnego
         */
        public double getMass() {
            return mass;
        }
        /**
         * Mutuator dla masy punktu
         *
         * @param mass masa punktu
         */
        public void setMass(double mass) {
            this.mass = mass;
        }
        /**
         * Konstruktor dla punktu materialnego
         * @param x X coordinate
         * @param y Y coordinate
         * @param z Z coordinate
         * @param mass Mass of the point
         */
        public Point(double x, double y, double z, double mass) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.mass = mass;
        }
        /**
         * Domyślny konstruktor
         */
        public Point() {
        }
    }
}
