
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Entity bean reprezentujący powiądzanie między studentem a przedmiotem
 * z tabeli Tbl_StudentCourse
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "Tbl_StudentCourse")
@IdClass(StudentCourseEntityId.class)
public class StudentCourseEntity implements Serializable {
    private static final long serialVersionUID = -1L;
    
    @Id
    private long studentId;
    @Id
    private long courseId;

    private int mark;

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "studentId", referencedColumnName = "Id")
    private StudentEntity student;

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "courseId", referencedColumnName = "Id")
    private CourseEntity course;

    /**
     * Konstruktor bezargumentowy
     */
    public StudentCourseEntity() {

    }

    /**
     * Getter - ocena studenta z danego przedmiotu
     *
     * @return ocena studenta z danego przedmiotu
     */
    public int getMark() {
        return mark;
    }

    /**
     * Settter - ocena studenta z danego przedmiotu
     *
     * @param mark ocena studenta z danego przedmiotu
     */
    public void setMark(int mark) {
        this.mark = mark;
    }

    /**
     * Getter - student
     *
     * @return student
     */
    public StudentEntity getStudent() {
        return student;
    }

    /**
     * Settter - student
     *
     * @param student student
     */
    public void setStudent(StudentEntity student) {
        this.student = student;
    }

    /**
     * Getter - przedmiot
     *
     * @return przedmiot
     */
    public CourseEntity getCourse() {
        return course;
    }

    /**
     * Setter przedmiot
     *
     * @param course przedmiot
     */
    public void setCourse(CourseEntity course) {
        this.course = course;
    }
}
