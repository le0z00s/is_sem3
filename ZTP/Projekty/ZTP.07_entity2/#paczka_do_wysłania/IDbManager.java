package pl.jrj.db;

import javax.ejb.Remote;

/**
 * @author Jerzy Jaworowski
 */
@Remote
public interface IDbManager {
    public boolean register(int hwork, String album); // hwork - numer zadania, album – numer albumu studenta
} 