
import java.io.Serializable;

/**
 * Klasa reprezentująca złożony klucz główny tabeli Tbl_StudentCourse
 * @author Rafał Zbojak
 */
public class StudentCourseEntityId implements Serializable {
    private static final long serialVersionUID = -1L;
    
    private long studentId;
    private long courseId;

    /**
     * Metoda generująca hashCode. Wynik musi być ten sam dla dwóch obiektów,
     * dla których metoda equals zwraca true.
     *
     * @return hashCode hashCode
     */
    @Override
    public int hashCode() {
        return (int) (studentId + courseId);
    }

    /**
     * Metoda służąca do sprawdzania, czy obiekt podany jako parametr
     * jest tym samym kluczem głównym
     *
     * @param obj obiekt do sprawdzenia
     * @return true, jeżeli sprawdzany obiekt jest tym samym kluczem głównym
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StudentCourseEntityId) {
            StudentCourseEntityId oId = (StudentCourseEntityId) obj;
            return (oId.studentId == this.studentId) &&
                    (oId.courseId == this.courseId);
        }
        return false;
    }
}
