import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity bean reprezentujący studenta
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "Tbl_Students")
public class StudentEntity implements Serializable {
    private static final long serialVersionUID = -1L;
    
    @Id
    @Column(name = "Id")
    private long id;
    private String firstName;
    private String lastName;

    @OneToMany(mappedBy = "student")
    private List<StudentCourseEntity> courses;

    /**
     * Konstruktor bezargumentowy
     */
    public StudentEntity() {

    }

    /**
     * Getter - id studenta
     *
     * @return id studenta
     */
    public long getId() {
        return id;
    }

    /**
     * Setter - id studenta
     *
     * @param id id studenta
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Getter - imię studenta
     *
     * @return imię studenta
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter - imię studenta
     *
     * @param firstName imię studenta
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter - nazwisko studenta
     *
     * @return nazwisko studenta
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter - nazwisko studenta
     *
     * @param lastName nazwisko studenta
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter - powiązania między studentem a przedmiotami, z których
     * otrzymał ocenę
     *
     * @return powiązania między studentem a przedmiotami, z których
     * otrzymał ocenę
     */
    public List<StudentCourseEntity> getCourses() {
        return courses;
    }

    /**
     * Setter - powiązania między studentem a przedmiotami, z których
     * otrzymał ocenę
     *
     * @param courses powiązania między studentem a przedmiotami, z których
     *                otrzymał ocenę
     */
    public void setCourses(List<StudentCourseEntity> courses) {
        this.courses = courses;
    }
}
