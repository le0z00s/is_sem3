
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.jrj.db.IDbManager;

/**
 * Klasa klienta. Ma za zadanie określić czy wynik uzyskany
 * przez studenta z danego przedmiotu jest równy, większy czy mniejszy
 * niż mediana ocen z tego przedmiotu
 * @author Rafał Zbojak
 */
public class AppMain {
    private EntityManager em;
    
    private static final String jndiString = "java:global/"
            + "ejb-project/DbManager!pl.jrj.db.IDbManager";
    
    private static final int taskNumber = 7;
    private static final String indexNumber = "93514";
    private static final String persistencePath = "myPersistence";
    
    /**
     * Konstruktor bezargumentowy
     */
    public AppMain() {
        EntityManagerFactory emf = Persistence
                .createEntityManagerFactory(persistencePath);
        this.em = emf.createEntityManager();
    }
    
    /**
     * Głowna metoda programu,
     * ładuje dane z pliku wejściowego 
     *
     * @param args argumenty linii komend
     */
    public static void main(String[] args) {
        String inputFile = args.length > 0 ? args[0] : null;
        double result = 0.0;

        try {
            AppMain main = new AppMain();
            InputFileReader ifr = new InputFileReader(inputFile);
            if (main.register(taskNumber, indexNumber)) {
                String course = ifr.getCourseName();
                String firstName = ifr.getFirstName();
                String lastName = ifr.getLastName();
                result = main.check(course, firstName, lastName) * 100;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.format("%.1f%%", result);
    }
    
    /**
     * 
     * @param hwork numer zadania
     * @param album numer albumu studenta
     * @return true jeśli pomyślnie zarejestrowano studenta
     * @throws NamingException wyjątek wyrzucany gdy nie powiodła się
     * operacja wyszukiwania komponentu w usłudze nazw
     */
    private static boolean register(int hwork, String album)
            throws NamingException {
        IDbManager dbManager;
        InitialContext context = new InitialContext();
        
        dbManager = (IDbManager) context.lookup(jndiString);
        return dbManager.register(hwork, album);
    }
    
    /**
     * Metoda oblicza medianę ze zbioru wartości
     * @param values zbiór wartości do sprawdzenia
     * @return mediana z podanego zbioru
     */
    private double getMedian(int[] values) {
        int midIndex = values.length / 2;
        
        Arrays.sort(values);
        
        if (values.length % 2 == 1) {
            return (double) values[midIndex];
        }
        return (double) (values[midIndex - 1] + values[midIndex]) / 2.0;
    }
    
    /**
     * Metoda umożliwiająca pobranie obiektu reprezentującego przedmiot
     * @param courseName nazwa przedmiotu
     * @return obiekt reprezentujący przedmiot
     * @throws Exception wyjątek rzucany, 
     * jeśli nie znaleziono przedmiotu o podanej nazwie
     */
    private CourseEntity getCourse(String courseName) throws Exception {
        List<CourseEntity> courses = em
                .createNamedQuery("CourseEntity.findByName",
                        CourseEntity.class)
                .setParameter("courseName", courseName)
                .getResultList();

        if (courses.size() == 0) {
            throw new Exception(String.format("Course %s not found", 
                    courseName));
        }
        return courses.get(0);
    }
    
    /**
     * Metoda odpowiadającą na pytanie, 
     * czy wskazany student o imieniu firstName i nazwisku lastName 
     * z przedmiotu o nazwie courseName uzyskał ocenę punktową 
     * poniżej czy powyżej mediany ocen dla tego przedmiotu
     * @param courseName nazwa przedmiotu
     * @param firstName imię studenta
     * @param lastName nazwisko studenta
     * @return wartość wyrażoną w procentach i wskazującą 
     * o ile wynik indywidualny studenta jest różny od mediany
     * @throws Exception wyjątek wyrzucany jeśli nie znaleziono studenta
     * lub kursu
     */
    private double check(String courseName, 
            String firstName, String lastName) throws Exception {
        CourseEntity course = this.getCourse(courseName);
        int[] marks = new int[course.getStudents().size()];
        Integer studentMark = null;
        int index = 0;

        for (StudentCourseEntity sce : course.getStudents()) {
            marks[index++] = sce.getMark();
            if (sce.getStudent().getFirstName().equals(firstName)
                    && sce.getStudent().getLastName().equals(lastName)) {
                studentMark = sce.getMark();
            }
        }

        if (studentMark == null) {
            throw new Exception(String.format("Student %s %s not found", 
                    firstName, lastName));
        }

        return ((double) studentMark / this.getMedian(marks)) - 1.0;
    }
    
    /**
     * Klasa służąca do pobierania danych wejściowych z pliku
     * @author Rafał Zbojak
     */
    public static class InputFileReader {
        private final Pattern coursePattern = Pattern
            .compile("^\\s*(\\S+(\\s+\\S+)*)\\s*$");
        private final Pattern studentPattern = Pattern
            .compile("^\\s*(\\S+)\\s*(\\S+)\\s*$"); 
        
        private BufferedReader reader;
        private String courseName;
        private String firstName;
        private String lastName;
        
        /**
         * Metoda parsująca plik wejściowy i odczytująca dane
         *
         * @throws IOException wyjątek wyrzucany, kiedy nie udało się 
         * odczytać danych z pliku wejściowego
         * @throws Exception wyjątek wyrzucany, kiedy format danych w
         * pliku wejściowym jest niepoprawny
         */
        private void readFileContent() throws IOException, Exception{
            if (this.reader.ready()) {
                this.parseCourseName(this.reader.readLine());
            } else {
                throw new Exception("Cannot parse course line.");
            }

            if (this.reader.ready()) {
                this.parseStudentNames(this.reader.readLine());
            } else {
                throw new Exception("Cannot parse student line.");
            }
        }

        /**
         * Metoda parsująca linię, 
         * w której powinna być wpisana nazwa przedmiotu
         * @param line linia tekstu z pliku wejściowego
         * @throws Exception wyjątek rzucany, kiedy format danych w
         * linii jest niepoprawny
         */
        private void parseCourseName(String line) throws Exception {
            Matcher matcher = this.coursePattern.matcher(line);
            if (!matcher.matches()) {
                throw new Exception("Unable to parse course name");
            }
            this.courseName = matcher.group(1);
        }

        /**
         * Metoda parsująca linię, 
         * w której powinno być wpisane imię i nazwisko studenta
         *
         * @param line linia tekstu z pliku wejściowego
         * @throws Exception wyjątek rzucany, kiedy format danych w
         * linii jest niepoprawny
         */
        private void parseStudentNames(String line) throws Exception {
            Matcher matcher = this.studentPattern.matcher(line);
            if (!matcher.matches()) {
                throw new Exception("Unable to parse student names");
            }

            this.firstName = matcher.group(1);
            this.lastName = matcher.group(2);
        }

        /**
         * Konstruktor klasy
         *
         * @param filename nazwa pliku
         * @throws IOException wyjątek wyrzucany, 
         * kiedy nie udało się otworzyć pliku
         * @throws Exception wyjątek rzucany, kiedy format danych w
         * pliku wejściowym jest niepoprawny
         */
        public InputFileReader(String filename) throws IOException,
                Exception {
            this.reader = new BufferedReader(
                    new FileReader(filename == null ? "" : filename));
            this.readFileContent();
            this.reader.close();
        }

        /**
         * Getter - imię studenta
         *
         * @return imię studenta
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * Getter - nazwisko studenta
         *
         * @return nazwisko studenta
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * Getter - nazwa przedmiotu
         *
         * @return nazwa przedmiotu
         */
        public String getCourseName() {
            return courseName;
        }
    }
}
