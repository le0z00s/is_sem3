
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity bean reprezentujący rekord z tabeli Tbl_Courses
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "Tbl_Courses")
@NamedQueries({
    @NamedQuery(name = "CourseEntity.findByName",
            query = "SELECT c FROM CourseEntity c "
                    + "WHERE c.courseName = :courseName")
})
public class CourseEntity implements Serializable{
    private static final long serialVersionUID = -1L;
    
    @Id
    @Column(name = "Id")
    private long id;
    
    private String courseName;
    
    @OneToMany(mappedBy = "course")
    private List<StudentCourseEntity> students;
    
    /**
     * Obowiązkowy konstruktor bezargumentowy
     */
    public CourseEntity() {
        
    }

    /**
     * Getter - id przedmiotu
     *
     * @return id przedmiotu
     */
    public long getId() {
        return id;
    }

    /**
     * Setter - id przedmiotu
     *
     * @param id id przedmiotu
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Getter - nazwa przedmiotu
     *
     * @return nazwa przedmiotu
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * Setter - nazwa przedmiotu
     *
     * @param courseName nazwa przedmiotu
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * Getter - powiązania między przedmiotem a studentami, którzy
     * otrzymali z niego ocenę
     *
     * @return powiązania między przedmiotem a studentami, którzy
     * otrzymali z niego ocenę
     */
    public List<StudentCourseEntity> getStudents() {
        return students;
    }

    /**
     * Setter - powiązania między przedmiotem a studentami, którzy
     * otrzymali z niego ocenę
     *
     * @param students powiązania między przedmiotem a studentami, którzy
     *                 otrzymali z niego ocenę
     */
    public void setStudents(List<StudentCourseEntity> students) {
        this.students = students;
    }
    
}
