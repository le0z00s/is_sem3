import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.jrj.db.IDbManager;

/**
 * Główna klasa programu. Rejestruje studenta, a następnie łączy się z
 * zewnętrznym repozytorium danych i oblicza jaki procent posiadaczy danego
 * modelu samochodu nie posiadał określonego dnia ubezpieczenia na auto.
 *
 * @author Rafał Zbojak
 */
public class AppClient {

    private final EntityManager entityManager;
    private static final String JNDI_STRING
            = "java:global/ejb-project/DbManager!pl.jrj.db.IDbManager";
    private static final String INDEX_NUMBER = "93514";
    private static final String PERSISTANCE_PATH = "persistence93514";
    private static final int HWORK = 6;

    /**
     * Główny konstruktor programu. Nawiązuje połączenie z zewnętrznym
     * repozytorium danych zarejestrowanych w usłudze JPA
     */
    public AppClient() {
        EntityManagerFactory emf = Persistence
                .createEntityManagerFactory(PERSISTANCE_PATH);
        this.entityManager = emf.createEntityManager();
    }

    /**
     * Główna metoda klasy. Rejestruje studenta, a następnie ładuje dane z pliku
     * wejściowego i oblicza jaki procent posiadaczy danego modelu auta nie
     * posiadało na nie ubezpieczenia w dniu określonym jako parametr zawarty w
     * pliku wejściowym
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String file = args.length > 0 ? args[0] : null;
        double output = 0.0;
        try {
            AppClient client = new AppClient();
            InputFileReader reader = new InputFileReader(file);
            
            if(client.register(HWORK, INDEX_NUMBER, JNDI_STRING)) {
                String model = reader.getModel();
                Date date = reader.getDate();
                output = client.assessUninsured(model, date);
            }
        } catch (NamingException ex) {
            Logger.getLogger(AppClient.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AppClient.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
        System.out.format("%.1f%%", output);
    }
    
    /**
     * Metoda umożliwia znalezienie procentu zaewidencjonowanych 
     * w bazie klientów, którzy określonego dnia nie posiadali 
     * ważnego ubezpieczenia na wskazany model samochodu
     * @param model model samochodu
     * @param date szukana data
     * @return procent klientów, którzy wskazanego dnia nie posiadali 
     * ważnego ubezpieczenia na auto
     */
    private double assessUninsured(String model, Date date) {
        int allClients = this.entityManager
                .createNamedQuery("Customer.findByModel")
                .setParameter("model", model)
                .getResultList()
                .size();
        int insuredClients = this.entityManager
                .createNamedQuery("Customer.findByDateAndModel")
                .setParameter("model", model)
                .setParameter("date", date)
                .getResultList()
                .size();
        
        return 1.0 - ((double)insuredClients / allClients);
    }

    /**
     * Metoda umożliwiająca zarejestrowanie studenta
     *
     * @param hwork numer zadania
     * @param index numer albumu studenta
     * @param jndiString ścieżka do komponentu w usłudze JNDI
     * @return true jeśli rejestracja studenta przebiegła pomyślnie
     * @throws NamingException
     */
    private boolean register(int hwork, String index, String jndiString)
            throws NamingException {
        InitialContext context = new InitialContext();

        IDbManager manager = (IDbManager) context
                .lookup(jndiString);
        return manager.register(hwork, index);
    }

    /**
     * Klasa pobierająca dane wejściowe z pliku. Zawiera metody parsujące ciągi
     * znaków oraz sprawdzające czy dane wejściowe są poprawne
     */
    public static class InputFileReader {

        private final Pattern modelPattern = Pattern
                .compile("^\\s*(\\S+(\\s+\\S+)*)\\s*$");
        private final Pattern datePattern = Pattern
                .compile("^\\s*(\\d\\d)/(\\d\\d)/(\\d\\d\\d\\d)\\s*$");
        private Date date;
        private String model;
        private BufferedReader reader;

        /**
         * Główny konstruktor klasy
         *
         * @param filename ścieżka do pliku wejściowego
         * @throws FileNotFoundException wyjątek wyrzucany w momencie
         * nieznalezienia pliku wejściowego
         * @throws Exception wyjątek wyrzucany w przypadku problemów z
         * parsowaniem pliku wejściowego
         */
        public InputFileReader(String filename)
                throws FileNotFoundException, Exception {
            this.reader = new BufferedReader(
                    new FileReader(
                            filename.trim().equals("") ? "" : filename));
            this.read();
            this.reader.close();
        }

        /**
         * Metoda czytająca zawartość pliku
         *
         * @throws IOException
         * @throws Exception
         */
        private void read() throws IOException, Exception {
            if (this.reader.ready()) {
                this.parseModel(this.reader.readLine().trim());
            } else {
                throw new Exception("Error while parsing model. Line empty");
            }

            if (this.reader.ready()) {
                this.parseDate(this.reader.readLine().trim());
            } else {
                throw new Exception("Error while parsing date. Line empty");
            }
        }

        /**
         * Metoda sprawdza czy linia zawiera model auta, a następnie przepisuje
         * dane
         *
         * @param line linia pliku wejściowego do sprawdzenia
         * @throws Exception
         */
        private void parseModel(String line) throws Exception {
            Matcher matcher = this.modelPattern.matcher(line);
            if (!matcher.matches()) {
                throw new Exception("Unable to parse model");
            }
            this.model = matcher.group(1);
        }

        /**
         * Metoda sprawdza czy linia zawiera poprawną datę, 
         * a następnie zapisuje dane
         * @param line linia pliku wejściowego do sprawdzenia
         * @throws Exception
         */
        private void parseDate(String line) throws Exception {
            Matcher matcher = this.datePattern.matcher(line);
            if (!matcher.matches()) {
                throw new Exception("Unable to parse date");
            }
            this.date = Date.valueOf(
                    String.format("%s-%s-%s", 
                            matcher.group(3),
                            matcher.group(2),
                            matcher.group(1)));
        }

        /**
         * Getter daty
         * @return data w formacie MM-dd-YYYY
         */
        public Date getDate() {
            return date;
        }

        /**
         * Getter modelu auta
         * @return model auta
         */
        public String getModel() {
            return model;
        }
    }
}
