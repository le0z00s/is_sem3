package pl.jrj.db;

import javax.ejb.Remote;

/**
 * Interfejs komponentu DbManager
 * Komponent resjestruje studenta
 * @author Rafał Zbojak
 */
@Remote
public interface IDbManager {
    /**
     * Metoda rejestruje studenta
     * @param hwork numer zadania
     * @param album numer albumu studenta
     * @return true jeśli rejestracja przebiegła pomyślnie
     */
    public boolean register(int hwork, String album);
}
