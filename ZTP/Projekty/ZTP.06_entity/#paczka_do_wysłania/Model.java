import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity bean modelu auta
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "TbModel")
public class Model implements Serializable {
    public static final long serialVersionUID = -1L;
    
    @Id
    @Column(name = "Id")
    private long id;
    private String model;

    /**
     * Obowiązkowy konstruktor bezargumentowy
     */
    public Model() {

    }

    /**
     * Getter id modelu auta
     * @return id auta
     */
    public long getId() {
        return id;
    }

    /**
     * Setter id auta
     * @param id id auta
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter nazwy auta
     * @return nazwa auta
     */
    public String getModel() {
        return model;
    }

    /**
     * Setter nazwy auta
     * @param model nazwa auta
     */
    public void setModel(String model) {
        this.model = model;
    }
}
