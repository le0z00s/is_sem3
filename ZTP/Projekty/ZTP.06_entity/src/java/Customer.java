import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity bean klienta
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "TbCustomer")
@NamedQueries({
    @NamedQuery(name = "Customer.findByDateAndModel",
                query = "SELECT COUNT(c) " +
                        "FROM Insurance i " +
                        "JOIN i.customer c " +
                        "JOIN i.model m " +
                        "WHERE i.dateFrom <= :date " +
                        "AND i.dateTo >= :date " +
                        "AND m.model = :model " +
                        "GROUP BY c"),
    @NamedQuery(name = "Customer.findByModel",
                query = "SELECT COUNT(c) " +
                        "FROM Insurance i " +
                        "JOIN i.customer c " +
                        "JOIN i.model m " +
                        "WHERE m.model = :model " +
                        "GROUP BY c")
})
public class Customer implements Serializable {
    public static final long serialVersionUID = -1L;
    
    @Id
    @Column(name = "Id")
    private long id;
    private String firstName;
    private String lastName;

    /**
     * Obowiązkowy konstruktor bezargumentowy
     */
    public Customer() {

    }

    /**
     * Getter id klienta
     * @return id klienta
     */
    public long getId() {
        return id;
    }

    /**
     * Setter id klienta
     * @param id id klienta
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter imienia klienta
     * @return imię klienta
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter imienia klienta
     * @param firstName imię klienta
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter nazwiska klienta
     * @return nazwisko klienta
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter nazwiska klienta
     * @param lastName nazwisko klienta
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
