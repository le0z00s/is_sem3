import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity bean ubezpieczenia
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "TbInsurance")
public class Insurance implements Serializable {
    
    public static final long serialVersionUID = -1L;
    
    @Id
    @Column(name = "Id")
    private long id;
    private Date dateFrom;
    private Date dateTo;
    @ManyToOne
    @JoinColumn(name = "customerId")
    private Customer customer;
    @ManyToOne
    @JoinColumn(name = "modelId")
    private Model model;

    /**
     * Obowiązkowy konstruktor bezargumentowy
     */
    public Insurance() {

    }

    /**
     * Getter id ubezpieczenia
     * @return id ubezpieczenia
     */
    public long getId() {
        return id;
    }

    /**
     * Setter id ubezpieczenia
     * @param id id ubezpieczenia
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter - data zakupu ubezpieczenia
     * @return data zakupu ubezpieczenia
     */
    public Date getDateFrom() {
        return dateFrom;
    }

    /**
     * Setter - data zakupu ubezpieczenia
     * @param dateFrom data zakupu ubezpieczenia
     */
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    /**
     * Getter - data wygaśnięcia ubezpieczenia
     * @return data wygaśnięcia ubezpieczenia
     */
    public Date getDateTo() {
        return dateTo;
    }

    /**
     * Setter - data wygaśnięcia ubezpieczenia
     * @param dateTo data wygaśnięcia ubezpieczenia
     */
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    /**
     * Getter id id klienta
     * @return id klienta
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Setter id klienta
     * @param customer id klienta
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Getter id auta
     * @return id auta
     */
    public Model getModel() {
        return model;
    }

    /**
     * Setter id auta
     * @param model id auta
     */
    public void setModel(Model model) {
        this.model = model;
    }
}
