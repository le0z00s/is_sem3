import java.awt.geom.Point2D;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Główna klasa programu. Realizuje połączenie do bazy danych
 * oraz przekazanie odczytanych danych do klasy Path.
 * Drukuje wynik obliczenia trasy o najniższym koszcie na konsolę.
 * @author Rafał Zbojak
 */
public class Client {
	private static final String query = "SELECT x, y, p FROM Data";
	private static Map<Point2D, Float> edges;

	/**
	 * Metoda realizujaca polaczenie z baza danych
	 * @param databaseString ołączenie z bazą danych
	 */
	public static void connect(final String databaseString) {
		try {
			Connection conn = DriverManager.getConnection(databaseString);
			Statement statement = conn.createStatement();

			prepareData(statement.executeQuery(query));

			statement.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metoda realizuje zapis odczytanych danych do listy krawędzi.
	 * @param result docelowa lista, do której zostaną zapisane dane
	 */
	private static void prepareData(final ResultSet result) throws SQLException {

		edges = new HashMap<Point2D, Float>();

		while (result.next()) {
			Point2D point = new Point2D.Float(result.getInt("x"), result.getInt("y"));
			edges.put(point, result.getFloat("p"));
		}
	}

	/**
	 * Metoda główna programu.
	 * @param args parametry wiersza poleceń
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			printResult(0f);
			return;
		}

		try {
			String databaseString = args[0].trim();
			int vertex = Integer.parseInt(args[1]);
			connect(databaseString);

			Path graph = new Path(edges);

			printResult(graph.optimalCost(vertex));
		} catch (Exception ex) {
			printResult(0f);
		}
	}

	/**
	 * Funkcja zwraca na standardowe wyjście obliczony koszt.
	 * @param value wartość do wyświetlenia na wyjściu.
	 */
	private static void printResult(float value) {
		System.out.format(Locale.US, "Koszt : %.3f", value);
	}

}
