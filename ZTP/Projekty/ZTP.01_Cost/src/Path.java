import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Klasa implementująca algorytm Dinica.
 * @author Rafał Zbojak
 */
public class Path {
	@SuppressWarnings("rawtypes")

	private static List safe(List list) {
		if (list == null) {
			return new ArrayList();
		}
		return list;
	}

	public Path(Map<Point2D, Float> data) {
		path = new ArrayList<Edge>();
		paths = new ArrayList<List<Edge>>();
		minCost = Float.MAX_VALUE;
		startingVertex = 1;
		createGraph(data);
	}

	// Lista wezlow sciezki w grafie
	private List<Edge> path;
	// Lista pokazujaca macierz sasiedztwa oraz graf
	private List<Edge>[] graph;
	// Lista sciezek
	private List<List<Edge>> paths;
	// Przedstawienie minimalny kosztu przejscia w grafie
	private float minCost;
	// Poczatkowy wezel
	private final int startingVertex;

	/**
	 * Metoda dodająca nową krawędź do grafu
	 * @param zr wierzchołek startowy
	 * @param tar wierzchołek końcowy
	 * @param capacity przepustowość
	 */
	public void addEdge(int zr, Integer trav, float capacity) {
		this.graph[zr].add(new Edge(trav, this.graph[trav].size(), capacity));
		this.graph[trav].add(new Edge(zr, this.graph[zr].size() - 1, 0f));
	}

	/**
	 * Metoda tworzaca pusta liste i przedstawia liste krawedzi grafu
	 * @param size ilość krawędzi grafu
	 */
	private List<Edge>[] prepareGraph(int size) {
		@SuppressWarnings("unchecked")
		List<Edge>[] graph = (List<Edge>[]) new List<?>[size];
		for (int i = 0; i < size; i++) {
			graph[i] = new ArrayList<Edge>();
		}
		return graph;
	}

	/**
	 * Funkca tworzaca nowy graf
	 * @param inputData dane wejściowe dla grafu
	 */
	private void createGraph(Map<Point2D, Float> inputData) {
		graph = prepareGraph(inputData.size() * 2);
		for (Entry<Point2D, Float> data : inputData.entrySet()) {
			addEdge((int) data.getKey().getX(), (int) data.getKey().getY(), data.getValue());
			addEdge((int) data.getKey().getY(), (int) data.getKey().getX(), data.getValue());
		}
	}

	/**
	 * Algorytm przeszukiwania grafu w głąb.
	 * Bada wszystkie krawędzie wychodzące z zadanego wierzchołka.
	 * @param tablica odległości do zadanego wierzchołka
	 * @param tar wierzchołek końcowy
	 * @param maxFlow przepływ
	 * @param ptr 
	 * @param roz
	 */
	private float DFS(int[] ptr, int[] roz, int tar, int u, float maxFlow) {
		if (u == tar)
			return maxFlow;
		for (; ptr[u] < this.graph[u].size(); ++ptr[u]) {
			Edge kr = this.graph[u].get(ptr[u]);

			if (roz[kr.getX()] == roz[u] + 1 && kr.getFlow() < kr.getCapacity()) {
				Float dfs = DFS(ptr, roz, tar, kr.getX(), Math.min(maxFlow, kr.getCapacity() - kr.getFlow()));
				if (dfs > 0) {
					path.add(kr);
					kr.setFlow(kr.getFlow() + dfs);
					this.graph[kr.getX()].get(kr.getY()).
						setFlow(this.graph[kr.getX()].get(kr.getY()).getFlow() - dfs);
					return dfs;
				}
			}
		}
		path.add(null);
		return 0f;
	}

	/**
	 * Algorytm przeszukiwania grafu wszerz.
	 * Przechodzi od zadanego wierzchołka, a mastępnie odwiedza
	 * wszystkie osiągalne z niego wierzchołki
	 * @param zr wierzchołek startowy
	 * @param tar wierzchołek końcowy
	 * @param tablica odległości do zadanego wierzchołka
	 */
	private boolean BFS(int zr, int tar, int[] roz) {
		Arrays.fill(roz, -1);
		roz[zr] = 0;
		int[] queue = new int[this.graph.length];
		int colSize = 0;
		queue[colSize++] = zr;

		for (int i = 0; i < colSize; i++) {
			int u = queue[i];
			for (Edge kr : this.graph[u]) {
				if (roz[kr.getX()] < 0 && kr.getFlow() < kr.getCapacity()) {
					roz[kr.getX()] = roz[u] + 1;
					queue[colSize++] = kr.getX();
				}
			}
		}
		return roz[tar] >= 0;
	}

	/**
	 * Metoda zwraca maksymalny przepływ
	 * @param zr wierzchołek startowy
	 * @param tar wierzchołek końcowy
	 */
	private void maxFlow(int zr, int tar) {
		int[] roz = new int[graph.length];

		while (BFS(zr, tar, roz)) {
			int[] ptr = new int[this.graph.length];
			while (true) {
				path = new ArrayList<Edge>();
				Float dfs = DFS(ptr, roz, tar, zr, Float.MAX_VALUE);
				paths.add(path);
				if (dfs == 0)
					break;
			}
		}
	}

	/**
	 * Metoda obliczająca optymalny koszt przejścia w grafie
	 * @param id wierzchołek końcowy
	 */
	public float optimalCost(int id) {
		maxFlow(startingVertex, id);
		float minCost = .0f;
		float minVertexCost = .0f;
		float minEdgeCost = 0.f;
		for (List<Edge> track : paths) {
			if (track.size() > 1) {
				Collections.reverse(track);
				float temp = .0f;
				float node = 0f;
				boolean ite = false;
				for (Object object : safe(track)) {
					Edge edge = (Edge) object;
					if (edge != null) {
						if (node == 0) {
							node = edge.getCapacity();
							temp = node;
						} else {
							minVertexCost += Math.abs(edge.getCapacity() - temp);
							temp = (float) edge.getCapacity();
							ite = true;
						}
						minEdgeCost += 1 / edge.getCapacity(); 
					}
				}
				minCost = minVertexCost + minEdgeCost;
				if (minCost < this.minCost && ite)
					this.minCost = minCost;
				else
					minCost = 0f;
			}
		}
		return this.minCost;
	}

	/**
	 * Klasa reprezentująca krawędź grafu.
	 */
	public class Edge {

		private Float capacity;
		private Float flow = 0f;
		private Integer x;
		private Integer y;

		/**
		 * Konstruktor
		 * @param x wierzchołek początkowy krawędzi
		 * @param y wierzchołek końcowy krawędzi
		 * @param capacity przepustowość krawędzi
		 */
		public Edge(Integer x, Integer y, Float capacity) {
			setX(x);
			setY(y);
			setCapacity(capacity);
		}

		/**
		 * Getter dla przepustowości krawędzi
		 */
		public Float getCapacity() {
			return capacity;
		}
		/**
		 * Setter dla przepustowości krawędzi
		 */
		public void setCapacity(Float capacity) {
			this.capacity = capacity;
		}
		/**
		 * Getter dla przepływu
		 */
		public Float getFlow() {
			return flow;
		}
		/**
		 * Setter dla przepływu
		 */
		public void setFlow(Float flow) {
			this.flow = flow;
		}
		/**
		 * Getter dla wierzchołka początkowego krawędzi
		 */
		public Integer getX() {
			return x;
		}
		/**
		 * Setter dla wierzchołka początkowego krawędzi
		 */
		public void setX(Integer x) {
			this.x = x;
		}
		/**
		 * Getter dla wierzchołka końcowego krawędzi
		 */
		public Integer getY() {
			return y;
		}
		/**
		 * Setter dla wierzchołka końcowego krawędzi
		 */
		public void setY(Integer y) {
			this.y = y;
		}
	}
}
