import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author rav
 */
public class ImageClient extends Application {
    
    private static final char INIT_SCALE = 10;
    private static final char END_SCALE = 50;
    
    char scale = INIT_SCALE + 1;
    boolean direction = true;
    
    @Override
    public void start(Stage primaryStage) {
        
        primaryStage.setOnCloseRequest(e -> Platform.exit());
        primaryStage.setTitle("ZTP Client - zad4");
        Group root = new Group();
        Scene scene = new Scene(root, 250, 250);
        primaryStage.setResizable(false);
        ImageView imageView = new ImageView(); 
        root.getChildren().add(imageView);
        
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                
                Image image = getImage(scale, scale);
                
                if(image == null){
                    this.cancel();
                }else{
                    if(scale > INIT_SCALE && scale < END_SCALE){
                        primaryStage.setWidth(image.getWidth());
                        primaryStage.setHeight(image.getHeight());
                        imageView.setImage(image);

                    }else{
                        direction = !direction;
                    }

                    if(direction){
                        scale++;
                    }else{
                        scale--;
                    }
                }
            }
        }, 5, 50);
        
        System.out.print(2/Math.pow(10, 8) + 1);
        
        primaryStage.setScene(scene);
        primaryStage.show();      
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    private void drawElements(){
        
    }
    
    public Image getImage(int width, int height){
        HttpURLConnection connection = null;
        Image image = null;
        try {
            String urlParameters = String.format("h=%d&v=%d",width,height);
            //Create connection

            URL url= new URL("localhost:8080/ImageServlet/ImageServlet?h=20&v=20");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", 
                 "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + 
                     Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");  

            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                connection.getOutputStream ());
            wr.writeBytes (urlParameters);
            wr.flush ();
            wr.close ();

            //Get Response    
            InputStream is = connection.getInputStream();
            //BufferedReader rd = new BufferedReader(new InputStreamReader(is));

            image = new Image(is);
        }catch(Exception e){
            System.err.println(e);
        }finally{
            connection.disconnect();
        }
        return image;
    }
}
