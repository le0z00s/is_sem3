import java.awt.Graphics2D;
import javax.ejb.Stateless;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Rafał Zbojak
 */
@Stateless
public class Scale implements IScale{

    @Override
    public BufferedImage scale(int width, int height, 
            String path) throws IOException {
        File file = new File(path);
        BufferedImage inputImage = ImageIO.read(file);
        
        
        width = (int) Math.ceil( width * inputImage.getWidth() / 100 );
        height = (int) Math.ceil( height * inputImage.getHeight() /100 );
        
        BufferedImage outputImage = 
                    new BufferedImage(width,
                height, inputImage.getType());
        
        Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0, width, 
                    height, null);
            g2d.dispose();
        
        return outputImage;
    }
    
}
