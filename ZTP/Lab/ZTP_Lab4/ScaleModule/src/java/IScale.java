
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.ejb.Remote;

/**
 *
 * @author Rafał Zbojak
 */
@Remote
public interface IScale {
    public BufferedImage scale(int width, int height, String path) 
            throws IOException;
}
