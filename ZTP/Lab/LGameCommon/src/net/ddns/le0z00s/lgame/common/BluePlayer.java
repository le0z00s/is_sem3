package net.ddns.le0z00s.lgame.common;

public class BluePlayer implements IPlayer {
    private static BluePlayer instance = null;

    private BluePlayer() {
        // private constructor
    }

    public static BluePlayer getInstance() {
        if (BluePlayer.instance == null) {
            BluePlayer.instance = new BluePlayer();
        }

        return BluePlayer.instance;
    }

    @Override
    public LPiece getLPiece(Board board) {
        return board.getBlueLPiece();
    }

    @Override
    public LPiece getOpponentsLPiece(Board board) {
        return board.getRedLPiece();
    }

    @Override
    public String getName() {
        return "Gracz niebieski";
    }

    @Override
    public String getShortName() {
        return "blue";
    }
}
