package net.ddns.le0z00s.lgame.common;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.setX(x);
        this.setY(y);
    }

    public Point(String movePart) throws IllegalMoveException {
        this(Point.getXFromString(movePart), Point.getYFromString(movePart));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        if (x < 0) {
            x = 0;
        }
        if (x > Board.MAX_X) {
            x = Board.MAX_X;
        }
        this.x = x;
    }

    public void setY(int y) {
        if (y < 0) {
            y = 0;
        }
        if (y > Board.MAX_Y) {
            y = Board.MAX_Y;
        }
        this.y = y;
    }

    public Point copy() {
        return new Point(this.x, this.y);
    }

    public int xDiff(Point p) {
        return this.x - p.getX();
    }

    public int yDiff(Point p) {
        return this.y - p.getY();
    }

    public boolean theSame(Point p) {
        return this.xDiff(p) == 0 && this.yDiff(p) == 0;
    }

    @Override
    public String toString() {
        String ret = "";
        switch (this.x) {
            case 0:
                ret += "A";
                break;
            case 1:
                ret += "B";
                break;
            case 2:
                ret += "C";
                break;
            case 3:
                ret += "D";
                break;
            default:
                ret += "X";
        }

        switch (this.y) {
            case 0:
                ret += "4";
                break;
            case 1:
                ret += "3";
                break;
            case 2:
                ret += "2";
                break;
            case 3:
                ret += "1";
                break;
            default:
                ret += "0";
        }

        return ret;
    }

    private static int getXFromString(String movePart) throws IllegalMoveException {
        switch (movePart.substring(0, 1)) {
            case "A":
                return 0;
            case "B":
                return 1;
            case "C":
                return 2;
            case "D":
                return 3;
            default:
                throw new IllegalMoveException();
        }
    }

    private static int getYFromString(String movePart) throws IllegalMoveException {
        switch (movePart.substring(1, 2)) {
            case "4":
                return 0;
            case "3":
                return 1;
            case "2":
                return 2;
            case "1":
                return 3;
            default:
                throw new IllegalMoveException();
        }
    }
}
