package net.ddns.le0z00s.lgame.common;

public class RedPlayer implements IPlayer {
    private static RedPlayer instance = null;

    private RedPlayer() {
        // private constructor
    }

    public static RedPlayer getInstance() {
        if (RedPlayer.instance == null) {
            RedPlayer.instance = new RedPlayer();
        }

        return RedPlayer.instance;
    }

    @Override
    public LPiece getLPiece(Board board) {
        return board.getRedLPiece();
    }

    @Override
    public LPiece getOpponentsLPiece(Board board) {
        return board.getBlueLPiece();
    }

    @Override
    public String getName() {
        return "Gracz czerwony";
    }

    @Override
    public String getShortName() {
        return "red";
    }
}
