package ztp.net.ddns.le0z00s;

import java.util.Map;
import java.util.HashMap;
import javax.ejb.Stateless;
import java.awt.image.BufferedImage;

/**
 *
 * @author Rafał Zbojak
 */
@Stateless
public class Scale implements IScale{

    @Override
    public Map<String, Integer> scale(int width, int height, BufferedImage image) {
        width = (int) Math.ceil( width * image.getWidth() / 100 );
        height = (int) Math.ceil( height * image.getHeight() /100 );
        Map<String, Integer> map = new HashMap<>();
        map.put("width", width);
        map.put("height", height);
        return map;
    }
    
}
