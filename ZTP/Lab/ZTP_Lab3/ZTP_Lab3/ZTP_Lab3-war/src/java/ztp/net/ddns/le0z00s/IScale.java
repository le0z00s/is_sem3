package ztp.net.ddns.le0z00s;
import java.awt.image.BufferedImage;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Rafał Zbojak
 */
@Local
public interface IScale {
    public Map<String, Integer> scale(int width, int height, BufferedImage image);
}
