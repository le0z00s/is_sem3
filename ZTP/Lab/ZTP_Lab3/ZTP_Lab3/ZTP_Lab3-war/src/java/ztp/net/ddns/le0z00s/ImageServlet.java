/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ztp.net.ddns.le0z00s;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rafał Zbojak
 */
public class ImageServlet extends HttpServlet {

    @EJB
    IScale scale;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, 
            HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("Mime");
        String filename = 
                "/home/rav/Obrazy/arctic-ice-penguins-2562131-1920x1200.jpg";
        
        try(OutputStream out = response.getOutputStream()){
            
            int vert = Integer.parseInt(request.getParameter("v"));
            int hor = Integer.parseInt(request.getParameter("h"));
            
            int h = vert > 0 ? vert : 100;
            int v = hor > 0 ? hor : 100;
            
            File file = new File(filename);
            
            BufferedImage inputImage = ImageIO.read(file);
            
            //Start EJB Component code fragment
            Map<String, Integer> imageScale = scale.scale(h, v, inputImage);
            h = imageScale.get("width");
            v = imageScale.get("height");
            //End EJB Component code fragment
            
            BufferedImage outputImage = 
                    new BufferedImage(h,
                v, inputImage.getType());
            
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0, h, 
                    v, null);
            g2d.dispose();
            
            ImageIO.write(outputImage, "jpg", out);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
