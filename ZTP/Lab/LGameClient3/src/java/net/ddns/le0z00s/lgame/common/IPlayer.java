package net.ddns.le0z00s.lgame.common;

/**
 * @author Rafał Zbojak
 */
public interface IPlayer {
    LPiece getLPiece(Board board);
    LPiece getOpponentsLPiece(Board board);
    String getName();
    String getShortName();
}
