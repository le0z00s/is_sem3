package net.ddns.le0z00s.lgame.common;

import java.util.*;

public class MoveGenerator {

    public static LinkedList<Move> getAllPossibleMoves(Board board, LPiece myLPiece, LPiece opponentsLPiece) {
        LinkedList<Move> moves = new LinkedList<>();
        LPiece myLPieceCopy = myLPiece.copy();

        LPiece myNewLPiece;
        SmallPiece myNewSmallPiece;

        LinkedList<Point> usedPoints = Utils.getAllPointsFromPieces(opponentsLPiece, board.getSmallPiece1(), board.getSmallPiece2());
        LinkedList<Point> otherPoints = Utils.getOtherPoints(usedPoints);
        LinkedList<Point> otherPoints1;
        LinkedList<Point> otherPoints2;

        for (Point p1 : otherPoints) {
            for (Point p2 : otherPoints) {
                if (p1.theSame(p2)) {
                    continue;
                }

                try {
                    myNewLPiece = new LPiece(p1, p2);
                } catch (IncorrectLPieceException e) {
                    continue;
                }

                if (myNewLPiece.theSame(myLPieceCopy)) {
                    continue;
                }

                try {
                    board.applyLPieceMove(myLPiece, myNewLPiece);
                } catch (IllegalMoveException e) {
                    continue;
                }

                otherPoints1 = Utils.getOtherPoints(Utils.getAllPointsFromPieces(myNewLPiece, opponentsLPiece, board.getSmallPiece2()));

                for (Point p : otherPoints1) {
                    myNewSmallPiece = new SmallPiece(p);
                    if (Config.MANDATORY_SMALL_PIECE_MOVE && myNewSmallPiece.theSame(board.getSmallPiece1())) {
                        continue;
                    }
                    moves.add(new Move(myNewLPiece, board.getSmallPiece1().copy(), myNewSmallPiece));
                }

                otherPoints2 = Utils.getOtherPoints(Utils.getAllPointsFromPieces(myNewLPiece, opponentsLPiece, board.getSmallPiece1()));

                for (Point p : otherPoints2) {
                    myNewSmallPiece = new SmallPiece(p);
                    if (Config.MANDATORY_SMALL_PIECE_MOVE && myNewSmallPiece.theSame(board.getSmallPiece2())) {
                        continue;
                    }
                    moves.add(new Move(myNewLPiece, board.getSmallPiece2().copy(), myNewSmallPiece));
                }
            }
        }

        try {
            board.applyLPieceMove(myLPiece, myLPieceCopy);
        } catch (IllegalMoveException e) {
            // this error should not occur
            e.printStackTrace();
        }

        return moves;
    }

    public static Move getMove(Board board, IPlayer player) {
        LPiece myLPiece = player.getLPiece(board);
        LPiece opponentsLPiece = player.getOpponentsLPiece(board);

        LinkedList<Move> myMoves1 = MoveGenerator.getAllPossibleMoves(board, myLPiece, opponentsLPiece);

        if (myMoves1.size() == 0) {
            return null;
        }

        int byThread = myMoves1.size() / Config.THREADS_NUM;
        int rest = myMoves1.size() % Config.THREADS_NUM;

        ArrayList<ArrayList<MoveWrapper>> movePortions = new ArrayList<>(Config.THREADS_NUM);
        Thread[] threads = new Thread[Config.THREADS_NUM];

        int startVal = 0;
        for (int i = 0; i < Config.THREADS_NUM; ++i) {
            int forThisThread = byThread;
            if (rest > 0) {
                rest--;
                forThisThread++;
            }

            ArrayList<MoveWrapper> thisThreadRes = new ArrayList<>(forThisThread);

            int endVal = startVal + forThisThread;
            List<Move> portion = myMoves1.subList(startVal, endVal);
            movePortions.add(i, thisThreadRes);

            threads[i] = new Thread(new ThreadRunner(thisThreadRes, portion, board, player));

            startVal = endVal;
        }

        for (int i = 0; i < Config.THREADS_NUM; ++i) {
            threads[i].start();
        }

        try {
            for (int i = 0; i < Config.THREADS_NUM; ++i) {
                threads[i].join();
            }
        } catch (InterruptedException e) {
            System.err.println(e.toString());
            return null;
        }

        ArrayList<MoveWrapper> moveWrappers = new ArrayList<>();
        int j = 0;
        for (ArrayList<MoveWrapper> movePortion : movePortions) {
            for (MoveWrapper moveWrapper : movePortion) {
                moveWrappers.add(moveWrapper);
            }
        }

        MoveWrapper[] array = moveWrappers.toArray(new MoveWrapper[moveWrappers.size()]);
        Arrays.sort(array);
        return array[0].getMove();
    }

    public static LinkedList<Move> simulate(Move move, Board board, IPlayer player, boolean mine) {
        LPiece myLPiece = player.getLPiece(board);
        LPiece opponentsLPiece = player.getOpponentsLPiece(board);

        LinkedList<Move> nextMoves;

        if (mine) {
            try {
                board.applyMove(move, myLPiece);
            } catch (IllegalMoveException e) {
                System.err.println(e.toString());
                return null;
            }

            nextMoves = MoveGenerator.getAllPossibleMoves(board, opponentsLPiece, myLPiece);
        } else {
            try {
                board.applyMove(move, opponentsLPiece);
            } catch (IllegalMoveException e) {
                System.err.println(e.toString());
                return null;
            }

            nextMoves = MoveGenerator.getAllPossibleMoves(board, myLPiece, opponentsLPiece);
        }

        return nextMoves;
    }
}

class MoveWrapper implements Comparable<MoveWrapper> {
    private Move move;
    private int possibleOpponentsMoves;
    private boolean bad = false;
    private boolean veryBad = false;

    private LinkedList<MoveWrapper> nextMovesWrappers;
    private LinkedList<Move> nextMoves;

    public MoveWrapper(Move move) {
        this.move = move;
    }

    public LinkedList<MoveWrapper> getNextMovesWrappers() {
        return nextMovesWrappers;
    }

    public void setNextMovesWrappers(LinkedList<MoveWrapper> nextMovesWrappers) {
        this.nextMovesWrappers = nextMovesWrappers;
    }

    public LinkedList<Move> getNextMoves() {
        return nextMoves;
    }

    public void setNextMoves(LinkedList<Move> nextMoves) {
        this.nextMoves = nextMoves;
    }

    public Move getMove() {
        return move;
    }

    public int getPossibleOpponentsMoves() {
        return possibleOpponentsMoves;
    }

    public void setPossibleOpponentsMoves(int possibleOpponentsMoves) {
        this.possibleOpponentsMoves = possibleOpponentsMoves;
    }

    public boolean isBad() {
        return bad;
    }

    public void setBad(boolean bad) {
        this.bad = bad;
    }

    public boolean isVeryBad() {
        return veryBad;
    }

    public void setVeryBad(boolean veryBad) {
        this.veryBad = veryBad;
    }

    @Override
    // 0 - takie same
    // -1 - this mniejsze
    // 1 - this wieksze
    public int compareTo(MoveWrapper c) {
        if (c.veryBad && !this.veryBad) {
            return -1;
        }

        if (!c.veryBad && this.veryBad) {
            return 1;
        }

        if (c.bad && !this.bad) {
            return -1;
        }

        if (!c.bad && this.bad) {
            return 1;
        }

        return (int) Math.signum(this.possibleOpponentsMoves - c.possibleOpponentsMoves);
    }

    public String toString() {
        return this.move.toString() + "(" + this.possibleOpponentsMoves + (this.bad ? ", bad" : "") + (this.veryBad ? ", very bad" : "") + ")";
    }
}

class ThreadRunner implements Runnable {
    private ArrayList<MoveWrapper> res;
    private List<Move> portion;
    private Board board;
    private IPlayer player;

    public ThreadRunner(ArrayList<MoveWrapper> res, List<Move> portion, Board board, IPlayer player) {
        this.res = res;
        this.portion = portion;
        this.board = board;
        this.player = player;
    }

    @Override
    public void run() {
        search:
        for (Move m1 : this.portion) {
            MoveWrapper mw = new MoveWrapper(m1);
            this.res.add(mw);

            Board boardCopy1 = this.board.copy();
            LinkedList<Move> opMoves1 = MoveGenerator.simulate(m1, boardCopy1, this.player, true);

            if (opMoves1 == null) {
                continue;
            }

            mw.setPossibleOpponentsMoves(opMoves1.size());

            if (Config.FAST_MODE && mw.getPossibleOpponentsMoves() == 0) {
                res.clear();
                res.add(mw);
                return;
            }

            mw.setNextMovesWrappers(new LinkedList<MoveWrapper>());

            for (Move o1 : opMoves1) {
                MoveWrapper o1mw = new MoveWrapper(o1);
                mw.getNextMovesWrappers().add(o1mw);

                Board boardCopy2 = boardCopy1.copy();
                LinkedList<Move> myMoves2 = MoveGenerator.simulate(o1, boardCopy2, this.player, false);

                if (myMoves2 == null) {
                    continue;
                }

                o1mw.setNextMoves(myMoves2);

                if (myMoves2.size() == 0) {
                    mw.setVeryBad(true);
                    mw.setNextMovesWrappers(null);
                    continue search;
                }
            }
        }

        if (Config.BE_VERY_CAREFUL) {
            search2:
            for (MoveWrapper mw1 : this.res) {
                if (mw1.isVeryBad()) {
                    continue;
                }

                Board boardCopy1 = this.board.copy();
                try {
                    boardCopy1.applyMove(mw1.getMove(), this.player.getLPiece(boardCopy1));
                } catch (IllegalMoveException e) {
                    System.err.println(e.toString());
                }

                for (MoveWrapper mw2 : mw1.getNextMovesWrappers()) {
                    Move o1 = mw2.getMove();
                    Board boardCopy2 = boardCopy1.copy();

                    try {
                        boardCopy2.applyMove(o1, this.player.getOpponentsLPiece(boardCopy2));
                    } catch (IllegalMoveException e) {
                        System.err.println(e.toString());
                    }

                    LinkedList<Move> myMoves2 = mw2.getNextMoves();

                    boolean notBadMoveExist = false;

                    for (Move m2 : myMoves2) {
                        Board boardCopy3 = boardCopy2.copy();
                        LinkedList<Move> opMoves2 = MoveGenerator.simulate(m2, boardCopy3, this.player, true);
                        if (opMoves2 == null) {
                            continue;
                        }

                        boolean bad = false;
                        for (Move o2 : opMoves2) {
                            Board boardCopy4 = boardCopy3.copy();
                            LinkedList<Move> myMoves3 = MoveGenerator.simulate(o2, boardCopy4, this.player, false);

                            if (myMoves3 == null) {
                                continue;
                            }

                            if (myMoves3.size() == 0) {
                                bad = true;
                            }
                        }

                        if (!bad) {
                            notBadMoveExist = true;
                            break;
                        }
                    }

                    if (!notBadMoveExist) {
                        mw1.setBad(true);
                        mw1.setNextMovesWrappers(null);
                        continue search2;
                    }
                }

                mw1.setNextMovesWrappers(null);

                if (Config.FAST_MODE && !mw1.isBad() && !mw1.isVeryBad()) {
                    res.clear();
                    res.add(mw1);
                    return;
                }
            }
        }
    }
}