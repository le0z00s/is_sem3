package net.ddns.le0z00s.lgame.common;

/**
 * @author Rafał Zbojak
 */
import java.util.LinkedList;

public class Board {
    public static final int MAX_X = 3;
    public static final int MAX_Y = 3;

    private final LPiece redLPiece;
    private final LPiece blueLPiece;
    private final SmallPiece smallPiece1;
    private final SmallPiece smallPiece2;

    public Board() {
        this.smallPiece1 = new SmallPiece(new Point(0, 0));
        this.smallPiece2 = new SmallPiece(new Point(3, 3));

        LPiece redLPiece = null;
        LPiece blueLPiece = null;

        try {
            redLPiece = new LPiece(new Point(1, 0), new Point(2, 2));
            blueLPiece = new LPiece(new Point(2, 3), new Point(1, 1));
        } catch (IncorrectLPieceException e) {
            // this error should not occur
            e.printStackTrace();
        }

        this.redLPiece = redLPiece;
        this.blueLPiece = blueLPiece;
    }

    public Board(SmallPiece smallPiece1, SmallPiece smallPiece2, LPiece redLPiece, LPiece blueLPiece) {
        this.smallPiece1 = smallPiece1;
        this.smallPiece2 = smallPiece2;
        this.redLPiece = redLPiece;
        this.blueLPiece = blueLPiece;
    }

    public Board copy() {
        return new Board(
                this.smallPiece1.copy(),
                this.smallPiece2.copy(),
                this.redLPiece.copy(),
                this.blueLPiece.copy()
        );
    }

    private SmallPiece getSmallPiece(SmallPiece sm) throws IllegalMoveException {
        return this.getSmallPiece(sm.getPosition());
    }

    private SmallPiece getSmallPiece(Point p) throws IllegalMoveException {
        Point pos = this.smallPiece1.getPosition();
        if (pos.getX() == p.getX() && pos.getY() == p.getY()) {
            return this.smallPiece1;
        }

        pos = this.smallPiece2.getPosition();
        if (pos.getX() == p.getX() && pos.getY() == p.getY()) {
            return this.smallPiece2;
        }

        throw new IllegalMoveException();
    }

    public void check() throws IllegalMoveException {
        LinkedList<Point> points = Utils.getAllPointsFromPieces(this.smallPiece1, this.smallPiece2, this.redLPiece, this.blueLPiece);

        int i = 0, j;
        for (Point p1 : points) {
            j = 0;
            for (Point p2 : points) {
                if (i != j && p1.theSame(p2)) {
                    throw new IllegalMoveException();
                }
                j++;
            }
            i++;
        }
    }

    public void applyLPieceMove(LPiece currentLPiece, LPiece newLPiece) throws IllegalMoveException {
        try {
            if (currentLPiece.theSame(newLPiece)) {
                throw new IllegalMoveException();
            }
            currentLPiece.move(newLPiece);
            this.check();
        } catch (IncorrectLPieceException | IllegalMoveException e) {
            throw new IllegalMoveException(e);
        }
    }

    public void applySmallPieceMove(SmallPiece smallPieceFrom, SmallPiece smallPieceTo) throws IllegalMoveException {
        if (Config.MANDATORY_SMALL_PIECE_MOVE && smallPieceFrom.theSame(smallPieceTo)) {
            throw new IllegalMoveException();
        }
        SmallPiece small = this.getSmallPiece(smallPieceFrom);
        small.move(smallPieceTo);
        this.check();
    }

    public void applyMove(Move move, LPiece lpiece) throws IllegalMoveException {
        try {
            this.applyLPieceMove(lpiece, move.getLPiece());
            this.applySmallPieceMove(move.getSmallPieceFrom(), move.getSmallPieceTo());
        } catch (IllegalMoveException e) {
            throw new IllegalMoveException(e);
        }
    }

    public void applyMove(Move move, IPlayer player) throws IllegalMoveException {
        this.applyMove(move, player.getLPiece(this));
    }

    public void applyMove(String moveString, IPlayer player) throws IllegalMoveException {
        this.applyMove(new Move(moveString), player);
    }

    public Move getTheBestMove(IPlayer player) {
        return MoveGenerator.getMove(this, player);
    }

    @Override
    public String toString() {
        String str = "";
        String[][] board = new String[Board.MAX_Y + 1][Board.MAX_X + 1];

        for (Point p : this.smallPiece1.getAllPoints()) {
            board[p.getY()][p.getX()] = "o";
        }

        for (Point p : this.smallPiece2.getAllPoints()) {
            board[p.getY()][p.getX()] = "o";
        }

        for (Point p : this.redLPiece.getAllPoints()) {
            board[p.getY()][p.getX()] = "R";
        }

        for (Point p : this.blueLPiece.getAllPoints()) {
            board[p.getY()][p.getX()] = "B";
        }

        for (int i = 0; i < board.length; ++i) {
            for (int j = 0; j < board[i].length; ++j) {
                if (board[i][j] == null || board[i][j].isEmpty()) {
                    str += " ";
                } else {
                    str += board[i][j];
                }
                if (j < board[i].length - 1) {
                    str += "  ";
                }
            }
            if (i < board.length) {
                str += "\n";
            }
        }

        return str;
    }

    public boolean theSame(Board b) {
        return this.redLPiece.theSame(b.getRedLPiece()) &&
                this.blueLPiece.theSame(b.getBlueLPiece()) &&
                (this.smallPiece1.theSame(b.getSmallPiece1()) || this.smallPiece2.theSame(b.getSmallPiece1())) &&
                (this.smallPiece1.theSame(b.getSmallPiece2()) || this.smallPiece2.theSame(b.getSmallPiece2()));
    }

    public String toRESTString() {
        return this.redLPiece.getShortEdge().toString() + "@" +
                this.redLPiece.getLongEdge().toString() + "@" +
                this.blueLPiece.getShortEdge().toString() + "@" +
                this.blueLPiece.getLongEdge().toString() + "@" +
                this.getSmallPiece1().getPosition().toString() + "@" +
                this.getSmallPiece2().getPosition().toString();
    }

    public static Board fromRESTString(String str) throws IllegalMoveException, IncorrectLPieceException {
        String[] parts = str.split("@");

        if (parts.length != 6) {
            throw new IllegalMoveException();
        }

        LPiece red = new LPiece(new Point(parts[0]), new Point(parts[1]));
        LPiece blue = new LPiece(new Point(parts[2]), new Point(parts[3]));
        SmallPiece small1 = new SmallPiece(new Point(parts[4]));
        SmallPiece small2 = new SmallPiece(new Point(parts[5]));

        return new Board(small1, small2, red, blue);
    }

    public LPiece getBlueLPiece() {
        return blueLPiece;
    }

    public LPiece getRedLPiece() {
        return redLPiece;
    }

    public SmallPiece getSmallPiece1() {
        return smallPiece1;
    }

    public SmallPiece getSmallPiece2() {
        return smallPiece2;
    }
}

