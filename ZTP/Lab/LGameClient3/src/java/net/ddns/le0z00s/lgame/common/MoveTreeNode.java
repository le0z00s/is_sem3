package net.ddns.le0z00s.lgame.common;

import java.util.LinkedList;

public class MoveTreeNode {
    private Move move;
    private LinkedList<MoveTreeNode> subnodes;

    public MoveTreeNode(Move move) {
        this.move = move;
        this.subnodes = new LinkedList<>();
    }

}
