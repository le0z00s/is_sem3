package net.ddns.le0z00s.lgame.common;

/**
 * @author Rafał Zbojak
 */
public class LPiece implements IPiece {
    Point shortEdge;
    Point longEdge;

    public LPiece(Point shortEdge, Point longEdge) throws IncorrectLPieceException {
        this.move(shortEdge, longEdge);
    }

    public void move(Point shortEdge, Point longEdge) throws IncorrectLPieceException {
        this.setPoints(shortEdge, longEdge);
    }

    public void move(LPiece to) throws IncorrectLPieceException {
        Point shortEdge = to.getShortEdge();
        Point longEdge = to.getLongEdge();
        this.move(shortEdge, longEdge);
    }

    public void setPoints(Point shortEdge, Point longEdge) throws IncorrectLPieceException {
        int xDiff = Math.abs(longEdge.xDiff(shortEdge));
        int yDiff = Math.abs(longEdge.yDiff(shortEdge));

        if (xDiff == 2 && yDiff == 1 || xDiff == 1 && yDiff == 2) {
            this.shortEdge = shortEdge;
            this.longEdge = longEdge;
            return;
        }

        throw new IncorrectLPieceException();
    }

    @Override
    public Point[] getAllPoints() {
        Point[] ret = new Point[4];

        ret[0] = this.shortEdge.copy();

        int xDiff = this.longEdge.xDiff(this.shortEdge);
        int yDiff = this.longEdge.yDiff(this.shortEdge);
        int x, y;

        if (Math.abs(xDiff) == 1) {
            y = this.shortEdge.getY();
            x = this.shortEdge.getX() + xDiff;
        } else {
            x = this.shortEdge.getX();
            y = this.shortEdge.getY() + yDiff;
        }

        ret[1] = new Point(x, y);
        ret[2] = new Point((ret[1].getX() + this.longEdge.getX()) / 2, (ret[1].getY() + this.longEdge.getY()) / 2);
        ret[3] = this.longEdge.copy();

        return ret;
    }

    @Override
    public boolean theSame(IPiece piece) {
        return piece instanceof LPiece && this.getShortEdge().theSame(((LPiece) piece).getShortEdge()) && this.getLongEdge().theSame(((LPiece) piece).getLongEdge());
    }

    public Point getShortEdge() {
        return shortEdge;
    }

    public void setShortEdge(Point shortEdge) {
        this.shortEdge = shortEdge;
    }

    public Point getLongEdge() {
        return longEdge;
    }

    public void setLongEdge(Point longEdge) {
        this.longEdge = longEdge;
    }

    @Override
    public String toString() {
        return this.shortEdge.toString() + this.longEdge.toString();
    }

    @Override
    public LPiece copy() {
        try {
            return new LPiece(this.shortEdge.copy(), this.longEdge.copy());
        } catch (IncorrectLPieceException e) {
            return null;
        }
    }
}
