package net.ddns.le0z00s.lgame.common;

public class Move {
    private LPiece lPiece;
    private SmallPiece smallPieceFrom;
    private SmallPiece smallPieceTo;

    public Move(String move) throws IllegalMoveException {
        Point[] movePoints = this.translateMove(move);
        Point lPieceShortPoint = movePoints[0];
        Point lPieceLongPoint = movePoints[1];
        Point smallPieceFromPoint = movePoints[2];
        Point smallPieceToPoint = movePoints[3];
        try {
            this.lPiece = new LPiece(lPieceShortPoint, lPieceLongPoint);
        } catch (IncorrectLPieceException e) {
            throw new IllegalMoveException(e);
        }
        this.smallPieceFrom = new SmallPiece(smallPieceFromPoint);
        this.smallPieceTo = new SmallPiece(smallPieceToPoint);
    }

    public Move(LPiece lPiece, SmallPiece smallPieceFrom, SmallPiece smallPieceTo) {
        this.lPiece = lPiece;
        this.smallPieceFrom = smallPieceFrom;
        this.smallPieceTo = smallPieceTo;
    }

    private Point[] translateMove(String move) throws IllegalMoveException {
        if (move == null || move.isEmpty() || move.length() != 8) {
            throw new IllegalMoveException();
        }

        String sMove;
        Point[] points = new Point[4];

        for (int i = 0; i < 4; ++i) {
            sMove = move.substring(2 * i, 2 * i + 2);
            points[i] = new Point(sMove);
        }

        return points;
    }

    public LPiece getLPiece() {
        return lPiece;
    }

    public void setLPiece(LPiece lPiece) {
        this.lPiece = lPiece;
    }

    public SmallPiece getSmallPieceFrom() {
        return smallPieceFrom;
    }

    public void setSmallPieceFrom(SmallPiece smallPieceFrom) {
        this.smallPieceFrom = smallPieceFrom;
    }

    public SmallPiece getSmallPieceTo() {
        return smallPieceTo;
    }

    public void setSmallPieceTo(SmallPiece smallPieceTo) {
        this.smallPieceTo = smallPieceTo;
    }

    @Override
    public String toString() {
        return this.lPiece.toString() +
                this.smallPieceFrom.toString() +
                this.smallPieceTo.toString();
    }
}
