package net.ddns.le0z00s.lgame.common;

import java.util.Arrays;
import java.util.LinkedList;

public class Utils {
    public static LinkedList<Point> getAllPointsFromPieces(IPiece... pieces) {
        LinkedList<Point> points = new LinkedList<>();

        for (IPiece piece : pieces) {
            points.addAll(Arrays.asList(piece.getAllPoints()));
        }

        return points;
    }

    public static LinkedList<Point> getOtherPoints(LinkedList<Point> points) {
        LinkedList<Point> otherPoints = new LinkedList<>();

        boolean[][] board = new boolean[Board.MAX_X + 1][Board.MAX_Y + 1];

        for (Point p : points) {
            board[p.getX()][p.getY()] = true;
        }

        for (int i = 0; i <= Board.MAX_X; ++i) {
            for (int j = 0; j <= Board.MAX_Y; ++j) {
                if (!board[i][j]) {
                    otherPoints.add(new Point(i, j));
                }
            }
        }

        return otherPoints;
    }

    public static void debug(String message) {
        if (!Config.DEBUG_ON) {
            return;
        }

        System.out.println(message + "\n");
    }
}
