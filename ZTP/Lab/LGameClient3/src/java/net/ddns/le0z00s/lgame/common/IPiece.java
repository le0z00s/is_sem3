package net.ddns.le0z00s.lgame.common;

public interface IPiece {
    Point[] getAllPoints();
    IPiece copy();
    boolean theSame(IPiece piece);
}
