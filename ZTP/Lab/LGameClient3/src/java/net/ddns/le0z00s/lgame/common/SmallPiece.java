package net.ddns.le0z00s.lgame.common;

public class SmallPiece implements IPiece {
    private Point position;

    public SmallPiece(Point position) {
        this.position = position;
    }

    @Override
    public Point[] getAllPoints() {
        Point[] ret = new Point[1];
        ret[0] = this.position.copy();

        return ret;
    }

    public Point getPosition() {
        return position;
    }

    public void move(Point position) {
        this.position = position;
    }

    public void move(SmallPiece sp) {
        Point position = sp.getPosition();
        this.move(position);
    }

    @Override
    public String toString() {
        return this.position.toString();
    }

    @Override
    public SmallPiece copy() {
        return new SmallPiece(this.position.copy());
    }

    @Override
    public boolean theSame(IPiece piece) {
        return piece instanceof SmallPiece && this.getPosition().theSame(((SmallPiece) piece).getPosition());
    }
}
