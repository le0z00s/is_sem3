import javax.naming.NamingException;

/**
 * @author Rafał Zbojak
 */
public class Client {

    public static void main(String[] args) {
        System.out.println("**************** LGame ****************");

        Player player1 = new Player("java:global/LGame3/LGame3-ejb/GameBean!net.ddns.le0z00s.lgame.ejb.IGame");
        Player player2 = new Player("java:global/LGame3/LGame3-ejb/GameBean!net.ddns.le0z00s.lgame.ejb.IGame");
        player1.setHostAndPort("gracz", 3700);
        player2.setHostAndPort("gracz", 3700);
        GameClient client = null;
        try {
            client = new GameClient(player1, player2);
        } catch (NamingException e) {
            e.printStackTrace();
            System.exit(1);
        }

        client.start();
    }
}
