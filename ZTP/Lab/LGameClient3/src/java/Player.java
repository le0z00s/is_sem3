/**
 * @author Rafał Zbojak
 */

public class Player {
    private String jndiName;
    private String host = null;
    private int port = -1;

    public Player(String jndiName) {
        this.setJNDIName(jndiName);
    }

    public Player(String jndiName, String host, int port) {
        this(jndiName);
        this.setHostAndPort(host, port);
    }

    public void setJNDIName(String jndiName) {
        this.jndiName = jndiName;
    }

    public String getJNDIName() {
        return this.jndiName;
    }

    public void setHostAndPort(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public String getHost() {
        return this.host;
    }

    public boolean isRemote(){
        return this.host != null || this.port != -1;
    }
}
