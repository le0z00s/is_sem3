package lgameclient;

import ejb.LGameBeanRemote;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class Main {
  
    private static LGameBeanRemote gameBean;
     
    public static List<String> generateAllPossibleMoves(String boardStr, int playerId) {
        
        List<String> moves = new ArrayList<>();
        
        char playerIdChar = (char)(playerId + '0');
        char EMPTY = '0';
        
        char[] board = boardStr.toCharArray();
        
        // jest 16 pol, pola sa ponumerowane o 0 do 15
        // w kazdej ponizszej linii pierwsze 4 elementy
        // to indeksy zajetych pol przez figure L
        // jesli jest ona w prawym gornym rogu
        // dwie kolejne wartosci to o ile moze sie przesunac poziomo
        // i pionowo
        // w ten sposob generujemy wszystkie mozwiwe polozenia L
        int[][] params =
        {
            {1, 5, 8, 9, 3, 2},
            {0, 4, 8, 9, 3, 2},
            {0, 1, 4, 8, 3, 2},
            {0, 1, 5, 9, 3, 2},
            {0, 4, 5, 6, 2, 3},
            {2, 4, 5, 6, 2, 3},   
            {0, 1, 2, 6, 2, 3},
            {0, 1, 2, 4, 2, 3}, 
        };
        
        // kierunki w jakich mozemy przesuwac gwiazdke
        int[] starDir = {-1, 1, -4, 4};
        
        for (int[] p : params) {
            
            int a = p[0];
            int b = p[1];
            int c = p[2];
            int d = p[3];
            
            int maxI = p[4];
            int maxJ = p[5];
 
            for (int i = 0; i < maxI; i++) {
                for (int j = 0; j < maxJ; j++) {
                    
                    int aindex = a + i + j * 4;
                    int bindex = b + i + j * 4;
                    int cindex = c + i + j * 4;
                    int dindex = d + i + j * 4;

                    char a1 = board[aindex];
                    char b1 = board[bindex];
                    char c1 = board[cindex];
                    char d1 = board[dindex];

                    boolean aCondition = a1 == EMPTY || a1 == playerIdChar;
                    boolean bCondition = b1 == EMPTY || b1 == playerIdChar;
                    boolean cCondition = c1 == EMPTY || c1 == playerIdChar;
                    boolean dCondition = d1 == EMPTY || d1 == playerIdChar;
                   
                    boolean notSamePosition = 
                            a1 == EMPTY ||
                            b1 == EMPTY ||
                            c1 == EMPTY ||
                            d1 == EMPTY;

                    if (aCondition && bCondition && cCondition && dCondition && notSamePosition) {
                        char[] move = boardStr.toCharArray();
                        
                        for (int k = 0; k < move.length; k++) {
                            if (move[k] == playerIdChar) {
                                move[k] = EMPTY;
                            }
                        }
                        
                        move[aindex] = playerIdChar;
                        move[bindex] = playerIdChar;
                        move[cindex] = playerIdChar;
                        move[dindex] = playerIdChar;
                        
                        moves.add(new String(move));
                        
                        // firstStar - pozycja pierszej gwiazdki
                        int firstStar = new String(move).indexOf("*");
                        int lastStar = new String(move).indexOf("*");
                        
                        for (int sd : starDir) {
                            
                            int newPos = firstStar + sd;
                            if (newPos >= 0 && newPos < 16 && move[newPos] == EMPTY) {
                              
                                char[] newMove = (char[])move.clone();
                                
                                newMove[firstStar] = EMPTY;
                                newMove[newPos] = '*';
                                
                                moves.add(new String(newMove));
                            } 
                            
                            newPos = lastStar + sd;
                            if (newPos >= 0 && newPos < 16 && move[newPos] == EMPTY) {
                              
                                char[] newMove = (char[])move.clone();
                                
                                newMove[firstStar] = EMPTY;
                                newMove[newPos] = '*';
                                
                                moves.add(new String(newMove));
                            } 
                        }
                    }
                }
            }
        }
        return moves;
    }
    
    private static void print(String move) {
        
        System.out.println(move.substring(0, 4));
        System.out.println(move.substring(4, 8));
        System.out.println(move.substring(8, 12));
        System.out.println(move.substring(12, 16));
    }
    
    public static void play(int playerId) {
        
        gameBean.register(playerId);
        
        System.out.println("Registered. Waiting for opponent...");
        
        Scanner scanner = new Scanner(System.in);
        
        while (true) {
            
            if (gameBean.isMyTurn(playerId)) {
                
                System.out.println("\nTurn for: " + playerId);
                String board = gameBean.getBoard();
                
                System.out.println("\nBoard:");
                print(board);
                
                List<String> moves = generateAllPossibleMoves(board, playerId);
                
                if (moves.isEmpty()) {
                    System.out.println("\nGAME OVER!");
                    return;
                }
                
                System.out.println("\nSelect move:");
                
                for (int i = 0; i < moves.size(); i++) {
                    System.out.println((i + 1) + ":");
                    print(moves.get(i));
                    System.out.println();
                }
                
                int choice = scanner.nextInt();
                
                String move = moves.get(choice);
                
                System.out.println("\nYour move:");
                print(move);
                
                if (generateAllPossibleMoves(move, playerId == 1 ? 2 : 1).isEmpty()) {
                    System.out.println("\nYOU WIN!");
                    return;
                }
                
                gameBean.sendMove(playerId, move);
                
            } else {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    
                }
            }
        }  
    }
    
    
    public static void main(String[] args) {
        
        InitialContext context;
        try {
            Properties props = new Properties();
            props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.sun.enterprise.naming.SerialInitContextFactory");
            props.setProperty(Context.URL_PKG_PREFIXES, "com.sun.enterprise.naming");
            props.setProperty(Context.STATE_FACTORIES, "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
            //props.setProperty(Context.PROVIDER_URL, "remote://192.168.1.4:3700");
            props.setProperty("org.omg.CORBA.ORBInitialHost", "gracz");
            props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
            context = new InitialContext(props);

            
            gameBean = (LGameBeanRemote) context.lookup("java:global/LGameServer/LGameBean!ejb.LGameBeanRemote");
            
            System.out.println("Select playerId (1 or 2):");
            
            
            // 1. normalna rozgrywka
            int playerId = new Scanner(System.in).nextInt();
            play(playerId);
            
                  
            /*
            // 2. szybki test dla dwoch graczy w jednym oknie
            new Thread(() -> {play(1);}).start();
            new Thread(() -> {play(2);}).start();
            */
 
        } catch (NamingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } 

    } 
}
