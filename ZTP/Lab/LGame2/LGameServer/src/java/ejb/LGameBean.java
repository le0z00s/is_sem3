
package ejb;

import javax.ejb.Stateful;


@Stateful(name = "LGameBean", mappedName = "LGameBean")
public class LGameBean implements LGameBeanRemote {

    int playerTurn = 1;  
    boolean player1Registered = false;
    boolean player2Registered = false;
    
    private String board = 
            "*110" + 
            "0210" + 
            "0210" + 
            "022*";
    
    private boolean playersReady() {
        return player1Registered && player2Registered;
    }
    
    @Override
    public boolean register(int playerId) {
        
        if (playerId == 1 && !player1Registered) {
            player1Registered = true;
            return true;
        } else if (playerId == 2 && !player2Registered) {
            player2Registered = true;
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isMyTurn(int playerId) {
        return playersReady() && playerTurn == playerId;
    }
    
    @Override
    public boolean sendMove(int playerId, String move) {   
        if (!playersReady()) {
            return false;
        } 
        board = move;
        playerTurn = playerTurn == 1 ? 2 : 1;
        return true;
    }
    
    @Override
    public String getBoard() {
        return board;
    }
}
