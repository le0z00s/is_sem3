
package ejb;
import javax.ejb.Remote;

@Remote
public interface LGameBeanRemote {
    
    boolean register(int playerId);
    boolean isMyTurn(int playerId);
    boolean sendMove(int player, String move); 
    String getBoard();
}
