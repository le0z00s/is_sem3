/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ztp.net.ddns.le0z00s;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Rafał Zbojak
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/MainServlet"})
public class MainServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, 
            HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        int majorVersion = session.getServletContext().getMajorVersion();
        int minorVersion = session.getServletContext().getMinorVersion();
        String info = session.getServletContext().getServerInfo();
        ServletContext context = request.getServletContext();
        
        //String filename = context.getRealPath(String.format("%simages/image.jpg",
        //        getServletContext().getRealPath(File.separator)));
        String filename = "/home/rav/Obrazy/RA3_Girls_black.jpg";
        String mime = context.getMimeType(filename);
        
        /*
        //ZADANIE 1
        try (PrintWriter out = response.getWriter()) {
            response.setContentType("text/html;charset=UTF-8");
        
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MainServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.format("<p>Servlet path: <b>%s%s</b></p>", 
            request.getContextPath(), request.getServletPath());
            
            out.format("<p>Servlet version: <b>%d.%d</b></p>", majorVersion, 
                    minorVersion);
            out.format("<p>Server info: <b>%s</b></p>", info);
            out.format("<p>Servlet context: <b>%s</b></p>", context);
            out.format("<p>Image path: <b>%s</b></p>", filename);
            
            out.println("</body>");
            out.println("</html>");
        }
        */
        
        /*
        //ZADANIE 2
        try(ServletOutputStream out = response.getOutputStream()){
            
            if (mime == null) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }

            response.setContentType(mime);
            File file = new File(filename);
            response.setContentLength((int)file.length());

            FileInputStream in = new FileInputStream(file);

            byte[] buf = new byte[1024];
            int count = 0;
            while ((count = in.read(buf)) >= 0) {
                out.write(buf, 0, count);
            }
            out.close();
            in.close();
        }
        */
        
        //ZADANIE 3
        //TODO Jebnąć skalowanie
        try(OutputStream out = response.getOutputStream()){
            
            int vert = Integer.parseInt(request.getParameter("v"));
            int hor = Integer.parseInt(request.getParameter("h"));
            
            int h = vert > 0 ? vert : 100;
            int v = hor > 0 ? hor : 100;
            
            File file = new File(filename);
            
            response.setContentType("text/html;charset=UTF-8");
            response.setContentType("Mime");
            
            BufferedImage inputImage = ImageIO.read(file);
            
            int width = (int) Math.ceil( h * inputImage.getWidth() / 100 );
            int height = (int) Math.ceil(v * inputImage.getHeight() /100 );
            
            BufferedImage outputImage = new BufferedImage(width,
                height, inputImage.getType());
            
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0, width, height, null);
            g2d.dispose();
            
            ImageIO.write(outputImage, "jpg", out);
            out.close();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
