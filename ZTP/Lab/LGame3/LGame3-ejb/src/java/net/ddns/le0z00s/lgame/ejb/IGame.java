package net.ddns.le0z00s.lgame.ejb;


import javax.ejb.Remote;

/**
 *
 * @author Rafał Zbojak
 */
@Remote
public interface IGame {
    String doMove(String move);
}