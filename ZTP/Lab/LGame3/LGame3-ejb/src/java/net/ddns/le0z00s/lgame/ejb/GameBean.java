package net.ddns.le0z00s.lgame.ejb;


import net.ddns.le0z00s.lgame.common.IllegalMoveException;
import net.ddns.le0z00s.lgame.common.Utils;
import net.ddns.le0z00s.lgame.common.Config;
import net.ddns.le0z00s.lgame.common.RedPlayer;
import net.ddns.le0z00s.lgame.common.IPlayer;
import net.ddns.le0z00s.lgame.common.Move;
import net.ddns.le0z00s.lgame.common.BluePlayer;
import net.ddns.le0z00s.lgame.common.Board;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Remote
@Stateful(name = "GameBean", mappedName = "GameBean")
public class GameBean implements IGame {
    private boolean initialized = false;
    private IPlayer player;
    private IPlayer opponent;
    private Board board;

    public GameBean() {
        reset();
    }

    private void init(boolean red) {
        if (red) {
            this.player = RedPlayer.getInstance();
            this.opponent = BluePlayer.getInstance();
        } else {
            this.player = BluePlayer.getInstance();
            this.opponent = RedPlayer.getInstance();
        }

        this.board = new Board();
        this.initialized = true;
    }

    private boolean isInitialized() {
        return this.initialized;
    }

    public void reset() {
        this.initialized = false;
    }

    private Move getMove(Board board, IPlayer player) throws IOException, IllegalMoveException {
        String boardStr = board.toRESTString();
        String playerStr = player.getShortName();

        HttpURLConnection connection = (HttpURLConnection) new URL("http://" + Config.REST_SERVICE + "/LGame/;board=" + boardStr + ";player=" + playerStr).openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        int code = connection.getResponseCode();

        if (code != 200) {
            throw new IllegalMoveException();
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String res = in.readLine();

        Utils.debug("Response from REST: " + res);

        if (res.equals("null")) {
            return null;
        }

        return new Move(res);
    }

    public String doMove(String state) {
        if (state == null && this.isInitialized()) {
            this.reset();
            Utils.debug("Jestem " + this.player.getName() + " i wygrałem!");
            return "I won!!!";
        }

        if (!this.isInitialized()) {
            this.init(state == null);
            Utils.debug("Jestem " + this.player.getName() + " i właśnie zaczynam.");
        } else {
            Utils.debug("Jestem " + this.player.getName() + " i kontynuuję grę.");
        }

        if (state != null) {
            state = state.toUpperCase();
            Utils.debug("Mój przeciwnik, " + this.opponent.getName() + " wykonał ruch " + state + ", który spróbuję u siebie narysować na takiej tablicy:\n" + this.board.toString());
            try {
                this.board.applyMove(state, this.opponent);
            } catch (IllegalMoveException e) {
                Utils.debug("Nie udało się! Mój przeciwnik to oszust!");
                return "You are a cheater!";
            }
        }

        Utils.debug("OK, moja tablica teraz wygląda tak:\n" + this.board.toString());

        Move theBestMove;

        try {
            theBestMove = getMove(this.board, this.player);
            Utils.debug("Ruch z REST: " + (theBestMove == null ? "null" : theBestMove.toString()));
        } catch (IOException | IllegalMoveException e) {
            theBestMove = this.board.getTheBestMove(this.player);
            Utils.debug("Ruch wygenerowany lokalnie: " + (theBestMove == null ? "null" : theBestMove.toString()));
        }

        if (theBestMove == null) {
            Utils.debug("Nie znalazłem dla siebie ruchu. Poddaję się.");
            return null;
        }

        Utils.debug("Mam ruch! Oto on: " + theBestMove.toString());

        try {
            this.board.applyMove(theBestMove, this.player);
        } catch (IllegalMoveException e) {
            // this error should not occur
            Utils.debug("Coś poszło mocno nie tak. Ten błąd nie powinien wystąpić. Mój ruch jest błędny.");
            e.printStackTrace();
            return null;
        }

        Utils.debug("Tablica po wykonaniu mojego ruchu wygląda tak:\n" + this.board.toString());
        Utils.debug("Koniec mojego ruchu. Zwracam: " + theBestMove.toString());

        return theBestMove.toString();
    }
}
