
import javax.ejb.Remote;

/**
 *
 * @author Rafał Zbojak
 */
@Remote
public interface IGame {
    String doMove(String move);
}