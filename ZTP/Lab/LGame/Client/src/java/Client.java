import javax.naming.NamingException;

/**
 * @author Rafał Zbojak
 */
public class Client {

    public static void main(String[] args) {
        System.out.println("**************** LGame ****************");

        Player player1 = new Player("java:global/LGame/Game!IGame");
        Player player2 = new Player("java:global/LGame/Game!IGame");
        player1.setHostAndPort("192.168.0.119", 3700);
        
        GameClient client = null;
        try {
            client = new GameClient(player1, player2);
        } catch (NamingException e) {
            e.printStackTrace();
            System.exit(1);
        }

        client.start();
    }
}
