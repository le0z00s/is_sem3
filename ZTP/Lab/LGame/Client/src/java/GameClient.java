import net.ddns.le0z00s.lgame.common.BluePlayer;
import net.ddns.le0z00s.lgame.common.Move;
import net.ddns.le0z00s.lgame.common.IPlayer;
import net.ddns.le0z00s.lgame.common.IllegalMoveException;
import net.ddns.le0z00s.lgame.common.RedPlayer;
import net.ddns.le0z00s.lgame.common.Board;
import net.ddns.le0z00s.lgame.common.MoveGenerator;
import javax.ejb.EJBException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.LinkedList;
import java.util.Properties;
import javax.naming.Context;

public class GameClient {
    private IGame player1;
    private IGame player2;

    private static IGame getPlayer(Player player) throws NamingException {
        InitialContext context;
        if (player.isRemote()) {
            Properties props = new Properties();
            props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.sun.enterprise.naming.SerialInitContextFactory");
            props.setProperty(Context.URL_PKG_PREFIXES, "com.sun.enterprise.naming");
            props.setProperty("java.naming.factory.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
            props.setProperty("org.omg.CORBA.ORBInitialHost", player.getHost());
            props.setProperty("org.omg.CORBA.ORBInitialPort", Integer.toString(player.getPort()));
            context = new InitialContext(props);
        } else {
            context = new InitialContext();
        }
        
        return (IGame) context.lookup(player.getJNDIName());
    }

    public GameClient(Player player1, Player player2) throws NamingException {
        System.out.println("Getting player 1...");
        this.player1 = GameClient.getPlayer(player1);
        System.out.println("Getting player 2...");
        this.player2 = GameClient.getPlayer(player2);
    }

    private GameClient() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private IGame switchCurrentPlayer(IGame game) {
        if (game == null) {
            return this.player1;
        }

        if (game == this.player1) {
            return this.player2;
        }

        return this.player1;
    }

    private IPlayer switchCurrentPlayerStrategy(IPlayer playerStrategy) {
        if (playerStrategy == null) {
            return RedPlayer.getInstance();
        }

        if (playerStrategy == RedPlayer.getInstance()) {
            return BluePlayer.getInstance();
        }

        return RedPlayer.getInstance();
    }

    private int checkHistory(LinkedList<Board> history, Board now) {
        int ret = 0;

        for (Board i : history) {
            if (i.theSame(now)) {
                ret++;
            }
        }

        history.add(now.copy());

        return ret;
    }

    public void start() {
        String move = null;

        IPlayer currentPlayerStrategy = this.switchCurrentPlayerStrategy(null);
        IGame currentPlayer = this.switchCurrentPlayer(null);

        Board board = new Board();
        LinkedList<Board> history = new LinkedList<>();

        do {
            System.out.println("Aktualny stan gry:");
            System.out.print(board);
            System.out.println("Ten stan wystąpił poprzednio " + this.checkHistory(history, board) + " raz(y)...");
            System.out.println();

            double duration = -1;

            try {
                long startTime = System.currentTimeMillis();
                move = currentPlayer.doMove(move);
                duration = (double) (System.currentTimeMillis() - startTime) / 1000d;
            } catch (EJBException e) {
                e.printStackTrace();
                System.out.println("Wygląda na to, że gracz " + currentPlayerStrategy.getName() + " się rozłączył...");
                move = null;
            }

            if (move == null) {
                System.out.println(currentPlayerStrategy.getName() + " poddał się!");
                System.out.println("Trwało to " + duration + "s");
            } else {
                System.out.println(currentPlayerStrategy.getName() + " wykonuje ruch: (" + move + ")");
                System.out.println("Trwało to " + duration + "s");

                try {
                    System.out.println("Teraz przeciwnik ma " + MoveGenerator.simulate(new Move(move), board.copy(), currentPlayerStrategy, true).size() + " możliwych ruchów!");
                    board.applyMove(move, currentPlayerStrategy);
                } catch (IllegalMoveException e) {
                    System.out.println(currentPlayerStrategy.getName() + " wykonał niedozwolony ruch. Koniec gry.");
                    move = null;
                }
            }

            currentPlayer = this.switchCurrentPlayer(currentPlayer);
            currentPlayerStrategy = this.switchCurrentPlayerStrategy(currentPlayerStrategy);
        } while (move != null);

        System.out.println("");
        System.out.println("***************************************");
        System.out.println("");

        System.out.println(currentPlayerStrategy.getName() + " wygrał :)");
    }
}