import net.ddns.le0z00s.lgame.common.BluePlayer;
import net.ddns.le0z00s.lgame.common.Utils;
import net.ddns.le0z00s.lgame.common.IPlayer;
import net.ddns.le0z00s.lgame.common.Move;
import net.ddns.le0z00s.lgame.common.RedPlayer;
import net.ddns.le0z00s.lgame.common.IncorrectLPieceException;
import net.ddns.le0z00s.lgame.common.Board;
import net.ddns.le0z00s.lgame.common.IllegalMoveException;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/")
@Produces("text/plain")
public class LGameService {
    @GET
    public Response doMove(@MatrixParam("board") String boardStr,
                           @MatrixParam("player") String playerStr) {

        if(boardStr == null || playerStr == null){
            return Response.status(400).entity("Incorrect input").build();
        }

        Utils.debug("[LGameService] Looking for move: " + boardStr + " for player " + playerStr);

        Board board;
        IPlayer player;

        try {
            board = Board.fromRESTString(boardStr);
            switch (playerStr) {
                case "red":
                    player = RedPlayer.getInstance();
                    break;
                case "blue":
                    player = BluePlayer.getInstance();
                    break;
                default:
                    throw new IllegalMoveException();
            }
        } catch (IllegalMoveException | IncorrectLPieceException e) {
            return Response.status(400).entity("Incorrect input").build();
        }

        Move theBestMove = board.getTheBestMove(player);

        if (theBestMove == null) {
            Utils.debug("[LGameService] Move: null");
            return Response.status(200).entity("null").build();
        }

        Utils.debug("[LGameService] Move: " + theBestMove.toString());
        return Response.status(200).entity(theBestMove.toString()).build();
    }
}