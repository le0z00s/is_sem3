package net.ddns.le0z00s.lgame.common;

public class IncorrectLPieceException extends Exception {
    public IncorrectLPieceException() {
        super();
    }

    public IncorrectLPieceException(String message) {
        super(message);
    }

    public IncorrectLPieceException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectLPieceException(Throwable cause) {
        super(cause);
    }
}
