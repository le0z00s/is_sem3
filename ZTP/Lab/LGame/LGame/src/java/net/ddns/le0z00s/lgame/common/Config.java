package net.ddns.le0z00s.lgame.common;

public class Config {
    public static final boolean DEBUG_ON = true;
    public static final boolean MANDATORY_SMALL_PIECE_MOVE = true;
    public static final boolean BE_VERY_CAREFUL = true;
    public static final boolean FAST_MODE = true;
    public static final int THREADS_NUM = 4;
    public static final String REST_SERVICE = "localhost:8080";
}
